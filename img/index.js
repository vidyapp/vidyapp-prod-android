export const bgmain = require('./bgmain.png');
export const sidenavBg = require('./sidenav-bg.png');
export const logoOne = require('./vidya_new_logo.png');
export const profileImg = require('./profileimg.png');
export const profileBg = require('./profile-bg.png');
export const emergencyLogo = require('./amount.png');
export const gemsLogo = require('./gems.png');
export const lfesLogo = require('./lfes.png');
export const newsBg = require('./bgmountain.png');
export const eventsBg = require('./bgsoccer.png');
export const gpaBg = require('./bgbooks.png');
export const notifImg = require('./schoolclosed.png');
export const newsCover = require('./newscover.png');
export const notifCover = require('./notificover.png');