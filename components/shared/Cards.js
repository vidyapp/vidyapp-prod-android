import React from "react";
import { StyleSheet, View } from "react-native";

export default function Card({ style, children }) {
	return (
		<View style={styles.card}>
			<View style={[styles.cardContent, style]}>{children}</View>
		</View>
	);
}
const styles = StyleSheet.create({
	card: {
		borderRadius: 25,
		backgroundColor: "#fff",
		marginBottom: 15,
	},
	cardContent: {
		padding: 20,
	},
});
