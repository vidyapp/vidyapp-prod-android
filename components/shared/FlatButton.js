import React from "react";
import { StyleSheet, TouchableOpacity, Text, View } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import * as Colors from "../../styles/colors";

export default function FlatButton({
	handleButtonPress,
	buttonColor,
	buttonLabel,
}) {
	return (
		<TouchableOpacity onPress={handleButtonPress}>
			<View style={[styles.button, { backgroundColor: buttonColor }]}>
				<Text style={styles.buttonText}>{buttonLabel}</Text>
			</View>
		</TouchableOpacity>
	);
}
const styles = StyleSheet.create({
	button: {
		borderRadius: 30,
		width: "100%",
		paddingVertical: RFValue(13),
		alignSelf: "center",
		marginBottom: 20,
	},
	buttonText: {
		color: "white",
		fontSize: RFValue(17),
		textAlign: "center",
		textTransform: "uppercase",
	},
});
