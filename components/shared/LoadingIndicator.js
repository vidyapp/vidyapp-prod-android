import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

import { Colors } from "../../styles";

function LoadingIndicator() {
	return (
		<View style={styles.loading}>
			<ActivityIndicator size="large" color={Colors.darkBlueTheme} />
		</View>
	);
}
const styles = StyleSheet.create({
	loading: {
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#F5FCFF88",
	},
});

export default LoadingIndicator;
