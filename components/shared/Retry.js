import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { Button } from "react-native-paper";
import { RFValue } from "react-native-responsive-fontsize";
import { Colors } from "../../styles";

function Retry({ handleRetry, message }) {
	return (
		<View style={styles.container}>
			<Text style={{ marginBottom: 20 }}>{message}</Text>
			<View style={{ width: "30%" }}>
				<Button
					mode="contained"
					color={Colors.grayTextBg}
					onPress={handleRetry}
				>
					Retry
				</Button>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: RFValue(10),
		backgroundColor: Colors.graywhite,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default Retry;
