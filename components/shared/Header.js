import React from "react";
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	StatusBar,
} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { Spacing, Typography } from "../../styles";
import { AuthContext } from "../Context";

export default function Header({
	navigation,
	title,
	icon,
	iconColor,
	AccountSwitcher,
}) {
	const { role } = React.useContext(AuthContext);
	function openMenu() {
		navigation.openDrawer();
	}
	function goToPreviousScreen() {
		navigation.goBack();
	}
	const action = () => {
		if (icon == "menu") {
			openMenu();
		} else {
			goToPreviousScreen();
		}
	};

	return (
		<View style={styles.header}>
			<StatusBar
				translucent={true}
				backgroundColor="rgba(0,0,0,0.2)"
				barStyle="light-content"
			/>
			{icon && (
				<TouchableOpacity onPress={action}>
					<Feather
						name={icon}
						size={24}
						style={[styles.headerIcon, { color: iconColor }]}
					/>
				</TouchableOpacity>
			)}

			<View
				style={{
					width: "85%",
				}}
			>
				<Text style={styles.headerTitle}>{title}</Text>
			</View>
			{role == "PARENT" && AccountSwitcher}
		</View>
	);
}

const styles = StyleSheet.create({
	header: {
		flex: 1,
		flexDirection: "row",
		padding: Spacing.base,
		height: 124,
		alignItems: "center",
	},

	headerTitle: {
		...Typography.screenHeader,
		textAlign: "center",
	},
	headerIcon: {
		color: "#fff",
		alignSelf: "flex-start",
	},
	bgImage: {
		resizeMode: "cover",
		justifyContent: "center",
		width: "100%",
		height: "100%",
	},
});
