import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { Colors } from "../styles";

export default function FloatingActionButton({ icon, onPress }) {
	return (
		<TouchableOpacity style={styles.button} onPress={onPress}>
			<Feather style={styles.icon} name={icon} size={30} color="#fff" />
		</TouchableOpacity>
	);
}
const styles = StyleSheet.create({
	button: {
		position: "absolute",
		bottom: 20,
		right: 25,
		alignSelf: "flex-end",
		backgroundColor: Colors.teal,
		alignItems: "center",
		justifyContent: "center",
		padding: 15,
		borderRadius: 30,
		elevation: 4,
	},

	icon: {},
});
