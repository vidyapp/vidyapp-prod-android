import React, { useState } from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Avatar, Title, Caption, Drawer } from "react-native-paper";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { sidenavBg, logoOne } from "../img/index";
import { Colors, Spacing, Typography } from "../styles";
import { AuthContext } from "./Context";
import DeviceInfo from 'react-native-device-info';

export default function CustomDrawerContent(props) {
	const {
		authContext: { signOut },
		activeProfile,
		server,
	} = React.useContext(AuthContext);
	const [renderSEP, setRenderSEP] = useState(false);
	let IMAGE_URL = { uri: activeProfile.image_original };
	// console.log('profile',activeProfile.image_original)
	let school = "Vidya";
	switch(server) {
		case "staging.vidyapp.co":
			school = "Geezerbuild Staging";
			break;
		case "app.vidyapp.co":
			school = "GEMS School";
			break;
		case "lfes.vidyapp.co":
			school = "LFES School";
			break;
	}
	const showSEP = (renderSEP) => {
		if (renderSEP) {
			return (
				<View style={styles.drawerSubMenu}>
					<DrawerItem
						label="Grades"
						labelStyle={styles.submenuLabel}
						onPress={() => {
							props.navigation.navigate("Grades");
						}}
					/>
					<DrawerItem
						label="Classes"
						labelStyle={styles.submenuLabel}
						onPress={() => {
							props.navigation.navigate("Classes");
						}}
					/>
					<DrawerItem
						label="Assignments"
						labelStyle={styles.submenuLabel}
						onPress={() => {
							props.navigation.navigate("Assignments");
						}}
					/>
					<DrawerItem
						label="Resources"
						labelStyle={styles.submenuLabel}
						onPress={() => {
							props.navigation.navigate("Resources");
						}}
					/>
					<DrawerItem
						label="My Attendance"
						labelStyle={styles.submenuLabel}
						onPress={() => {
							props.navigation.navigate("Attendance");
						}}
					/>
				</View>
			);
		}
	};
	return (
		<View style={{ flex: 1 }}>
			<Image style={styles.backgroundImage} source={sidenavBg}></Image>
			<DrawerContentScrollView {...props}>
				<View style={styles.drawerContent}>
					<View style={styles.userInfoSection}>
						<View style={{ flexDirection: "row", marginTop: 15 }}>
							<Avatar.Image source={IMAGE_URL} size={72} />
							{ false &&
								<Image
									style={styles.profileLogo}
									source={logoOne}
								></Image>
							}
							<View
								style={{
									flexDirection: "column",
									marginTop: -10,
									marginLeft: 15,
								}}
							>
								<Title style={styles.title}>
									{activeProfile.firstname}{" "}
									{activeProfile.lastname}
								</Title>
								<Caption style={styles.server}>
									{school}
								</Caption>
								<TouchableOpacity>
									<Caption
										style={styles.caption}
										onPress={() => {
											props.navigation.navigate(
												"Profile"
											);
										}}
									>
										View Profile
									</Caption>
								</TouchableOpacity>
							</View>
						</View>
					</View>
					<Drawer.Section style={styles.drawerSection}>
						<DrawerItem
							label="Dashboard"
							labelStyle={styles.labelStyle}
							onPress={() => {
								props.navigation.navigate("Home");
							}}
						/>
						<DrawerItem
							label="News and Notifications"
							labelStyle={styles.labelStyle}
							onPress={() => {
								props.navigation.navigate("News");
							}}
						/>
						<DrawerItem
							label="Calendar"
							labelStyle={styles.labelStyle}
							onPress={() => {
								props.navigation.navigate("Calendar");
							}}
						/>
						<DrawerItem
							label="Student Education Portal"
							labelStyle={styles.labelStyle}
							onPress={() => {
								if (renderSEP) {
									setRenderSEP(false);
								} else setRenderSEP(true);
							}}
						/>
						{showSEP(renderSEP)}
						<DrawerItem
							label="Finance"
							labelStyle={styles.labelStyle}
							onPress={() => {
								props.navigation.navigate("Finance");
							}}
						/>
						<DrawerItem
							label="Forms"
							labelStyle={styles.labelStyle}
							onPress={() => {
								props.navigation.navigate("Forms");
							}}
						/>
						<DrawerItem
							label="Contact"
							labelStyle={styles.labelStyle}
							onPress={() => {
								props.navigation.navigate("Contact");
							}}
						/>

						<DrawerItem
							label="Log Out"
							labelStyle={styles.labelStyle}
							onPress={() => {
								signOut();
							}}
						/>

						<DrawerItem
							label={DeviceInfo.getApplicationName() + ' (v.' + DeviceInfo.getVersion() + ' build ' + DeviceInfo.getBuildNumber() + ')'}
							labelStyle={styles.appVersionLabel}
							onPress={() => {
							}}
						/>
					</Drawer.Section>
				</View>
			</DrawerContentScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	drawerContent: {
		flex: 1,
	},
	backgroundImage: {
		position: "absolute",
		top: 0,
		left: -20,
		bottom: 0,
		zIndex: -1,
		height: "100%",
	},
	profileLogo: {
		position: "absolute",
		top: 0,
		left: 52,
		width: 26,
		height: 26,
	},
	userInfoSection: {
		paddingLeft: 20,
	},
	title: {
		marginTop: Spacing.smaller,
		...Typography.screenHeader,
	},
	server: {
		color: Colors.white,
		...Typography.regularSmallText,
	},
	caption: {
		color: Colors.white,
		lineHeight: Spacing.large,
		opacity: 0.7,
		...Typography.regularSmallText,
	},
	labelStyle: {
		...Typography.sidenavLinks,
	},
	row: {
		marginTop: 20,
		flexDirection: "row",
		alignItems: "center",
	},
	section: {
		flexDirection: "row",
		alignItems: "center",
		marginRight: 15,
	},
	paragraph: {
		fontWeight: "bold",
		color: "white",
		marginRight: 3,
	},
	drawerSection: {
		marginTop: 15,
	},
	drawerSubMenu: {
		marginLeft: 20,
	},
	submenuLabel: {
		...Typography.sidenavSublinks,
	},
	appVersionLabel: {
		...Typography.sidenavSublinks,
		fontSize: 12,
	},
});
