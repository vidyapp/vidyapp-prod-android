import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Colors } from "../styles";
import Card from "./shared/Cards";

function AttendanceListItem({ label, number, backgroundColor }) {
	return (
		<Card style={styles.container}>
			<Text style={styles.label}>{label}</Text>
			<View style={[styles.badge, { backgroundColor }]}>
				<Text style={styles.number}>{number}</Text>
			</View>
		</Card>
	);
}

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		alignItems: "center",
	},
	badge: {
		width: 55,
		height: 55,
		borderRadius: 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: Colors.green,
	},
	label: {
		flex: 1,
		fontSize: 18,
	},
	number: {
		color: "white",
		fontSize: 18,
		fontWeight: "400",
	},
});

export default AttendanceListItem;
