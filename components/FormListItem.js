import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Colors } from "../styles";
import Card from "./shared/Cards";

function FormListItem({ status, title, description, from, to }) {
	const badgeBgColor = () => {
		if (status == "PENDING") {
			return {
				backgroundColor: Colors.grayTextBg,
			};
		} else if (status == "APPROVED") {
			return {
				backgroundColor: Colors.greenTextBg,
			};
		} else {
			return {
				backgroundColor: Colors.pinkTextBg,
			};
		}
	};
	const badgeTextColor = () => {
		if (status == "PENDING") {
			return {
				color: Colors.grayText,
			};
		} else if (status == "APPROVED") {
			return {
				color: Colors.greenText,
			};
		} else {
			return {
				color: Colors.pinkText,
			};
		}
	};

	return (
		<Card style={styles.container}>
			<View style={[styles.badge, badgeBgColor()]}>
				<Text style={[styles.status, badgeTextColor()]}>{status}</Text>
			</View>
			<Text style={styles.title}>{title}</Text>
			<Text style={styles.description}>{description}</Text>
			<Text style={styles.date}>
				{from}
				{" to "}
				{to}
			</Text>
		</Card>
	);
}

const styles = StyleSheet.create({
	container: {},
	badge: {
		flexDirection: "row",
		alignSelf: "flex-start",
		paddingHorizontal: 8,
		paddingVertical: 2.5,
		borderRadius: 10,
	},
	status: {
		fontSize: 18,
		textTransform: "capitalize",
	},
	title: {
		marginVertical: 15,
		fontWeight: "bold",
		color: "#555",
		fontSize: 20,
	},
	description: {
		fontSize: 16,
	},
	date: {
		marginVertical: 10,
		color: Colors.grayText,
	},
});

export default FormListItem;
