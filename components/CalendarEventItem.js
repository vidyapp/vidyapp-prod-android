import React, { useEffect, useState } from "react";
import {
	View,
	Text,
	StyleSheet,
	LayoutAnimation,
	Platform,
	UIManager,
	TouchableOpacity,
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import { Colors } from "../styles";
import Card from "./shared/Cards";

function CalendarEventItem({ calEvent }) {
	const [expanded, setExpanded] = useState({});
	const [icon, setIcon] = useState({});

	useEffect(() => {
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
	}, []);

	const handleShortDetail = (item) => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		if (Object.keys(expanded).length == 0) {
			setExpanded({ [item]: true });
		} else {
			setExpanded((prevState) => ({
				...prevState,
				[item]: !prevState[item],
			}));
		}
		expanded[item]
			? setIcon({ ...icon, [item]: "chevron-down" })
			: setIcon({ ...icon, [item]: "chevron-up" });
	};

	const formatTime = (time) => {
		const t = time.split(":");
		return t[0] + ":" + t[1];
	};
	return (
		<Card>
			<View style={styles.duration}>
				<Text style={styles.time}>
					{formatTime(calEvent.start_time)} -{" "}
					{formatTime(calEvent.end_time)}
				</Text>
				<Text style={styles.date}>
					{calEvent.start_date} - {calEvent.end_date}
				</Text>
			</View>
			<TouchableOpacity
				style={styles.event}
				onPress={() => handleShortDetail(calEvent.id)}
			>
				<Text style={styles.title}>{calEvent.title}</Text>

				<Entypo name={icon[calEvent.id] || "chevron-down"} size={20} />
			</TouchableOpacity>
			<View
				style={{
					height: expanded[calEvent.id] || false ? null : 0,
					overflow: "hidden",
				}}
			>
				<Text>{calEvent.desc}</Text>
			</View>
		</Card>
	);
}

const styles = StyleSheet.create({
	duration: {
		flexDirection: "row",
		alignItems: "center",
		marginBottom: 10,
	},
	event: {
		flexDirection: "row",
		alignItems: "center",
	},
	date: {
		color: Colors.grayText,
	},
	time: {
		flex: 1,
		fontWeight: "bold",
		fontSize: 16,
		color: Colors.grayText,
	},
	title: {
		flex: 1,
		fontSize: 20,
	},
	desc: {},
});

export default CalendarEventItem;
