import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import * as Colors from "./colors";
export const globalStyles = StyleSheet.create({
	container: {
		flex: 1,
		padding: RFValue(10),
		backgroundColor: Colors.graywhite,
	},
});
