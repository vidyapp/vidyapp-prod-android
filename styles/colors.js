export const darkBlueTheme = "#333e97";
export const blueTheme = "#414ba2";
export const blackBlueTheme = "#303645";
export const grayBlueTheme = "#59566e";
export const lightBlueTheme = "#5660B2";
export const lighterBlueTheme = "#7B85CF";
export const green = "#53CBBB";
export const lightgrayTheme = "#989BA2";
export const greenBlueTheme = "#000000";
export const grayTheme = "#CBCDD0";
export const lightGray = "#EAEAED";
export const teal = "#51c0b0";
export const pink = "#E94D90";
export const lightwhite = "#F3F4F5";
export const graywhite = "#ECECEC";

export const iconBlue = "#1874BA";
export const iconBgBlue = "#7BA7CF";
export const iconPurple = "#3F4895";
export const iconBgPurple = "#7B85CF";

export const greenText = "#3EA99A";
export const greenTextBg = "#E0F4F1";
export const pinkText = "#E46FA1";
export const pinkTextBg = "#FFE6ED";
export const grayText = "#8B909A";
export const grayTextBg = "#F0F0F1";
export const errorText = "#b82834";

export const white = "#FFFFFF";
export const black = "#000000";
export const transparent = "rgba(0,0,0,0)";
export const dividercolor = "rgba(48, 54, 69, 0.08)";
