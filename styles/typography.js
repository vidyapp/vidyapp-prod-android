import * as Colors from './colors'
import * as Spacing from './spacing'

export const extraLargeFontSize = 32
export const largeFontSize = 24
export const buttonFontSize = 18
export const baseFontSize = 16
export const smallFontSize = 14
export const smallerFontSize = 12
export const smallestFontSize = 10
export const largeHeaderFontSize = 20
export const headerFontSize = 18

const base = {
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
}

export const labelHead = {
  color: Colors.grayBlueTheme,
  fontWeight: '400',
  fontSize:  baseFontSize,
}

export const inputText = {
  color: Colors.blackBlueTheme,
  fontWeight: 'normal',
  fontSize: baseFontSize,
}

export const sidenavLinks = {
  color: Colors.white,
  fontWeight: '500',
  fontSize: baseFontSize, 
}

export const sidenavSublinks = {
  color: Colors.white,
  fontWeight: '500',
  fontSize: smallFontSize,
}

export const regularSmallText = {
  fontWeight: 'normal',
  fontSize: smallFontSize,
}

export const link = {
  color: Colors.thoughtbotRed,
  fontWeight: 'bold',
}

export const lightsmallText = {
  color: Colors.lightgrayTheme,
  fontWeight: 'normal',
  fontSize: smallerFontSize,
}

export const screenHeader = {
  color: Colors.white,
  fontSize: largeHeaderFontSize,
  fontWeight: 'bold',
}

export const bodyHeader = {
  color: Colors.blackBlueTheme,
  fontSize: largeHeaderFontSize,
  fontWeight: 'bold',
}

export const blueTitlehead = {
  color: Colors.darkBlueTheme,
  fontSize: largeHeaderFontSize,
  fontWeight: 'bold',
}

export const bodyLabel = {
  color: Colors.blackBlueTheme,
  fontSize: baseFontSize,
  fontWeight: 'bold',
}

export const bodyText = {
  color: Colors.blackBlueTheme,
  fontSize: baseFontSize,
  fontWeight: 'normal',
}
