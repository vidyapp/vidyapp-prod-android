import React, { useState, useEffect } from "react";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import AuthStack from "./AuthStack";
import AppNavigator from "./AppNavigator";
import { AuthContext } from "../components/Context";
import { handleLogin } from "../api/Auth";
import { postDeviceId } from "../api/DataApi";
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import { Cache } from "react-native-cache";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {navigationRef} from './RootNavigation';

const cache = new Cache({
	    namespace: "loginData",
	    policy: {
	        maxEntries: 50000, // if unspecified, it can have unlimited entries
	        stdTTL: 0 // the standard ttl as number in seconds, default: 0 (unlimited)
	    },
	    backend: AsyncStorage
	});

export default function RootNavigator(props) {
	const [isLoggingIn, setIsLoggingIn] = useState(true);
	const [server, setServer] = useState(null);
	const [userToken, setUserToken] = useState(null);
	const [userId, setUserId] = useState();
	const [username, setUsername] = useState("");
	const [role, setRole] = useState("");
	const [responseMsg, setResponseMsg] = useState("");
	const [dialogVisible, setDialogVisible] = useState(false);
	const [students, setStudents] = useState([]);
	const [activeProfile, setActiveProfile] = useState({});

	const authContext = React.useMemo(
		() => ({
			signIn: async (server, username, password, is_Mobile) => {
				setIsLoggingIn(true);
				const userResponse = await handleLogin(
					server,
					username,
					password,
					is_Mobile
				);
				if (userResponse.status == 200) {
					if (!userResponse.data.error) {
						let userCredential = {
							server:server,
							username:username,
							password:password
						}
						const ddd = await cache.set("loginData", userCredential);
						const data = {
							user_id: userResponse.data.id,
							user_Token :  userResponse.data.access_token
						}	
						const reg = messaging().registerDeviceForRemoteMessages();
						messaging().getToken().then(async(token) =>{
							const response = await postDeviceId(server, token, data)
						})
						setServer(server);
						setUserToken(userResponse.data.access_token);
						setUserId(userResponse.data.id);
						setUsername(userResponse.data.firstname);
						if (userResponse.data.role === "PARENT") {
							setStudents(userResponse.data.student);
							setActiveProfile(userResponse.data.student[0]);
						} else {
							setActiveProfile(function () {
								let obj = {};
								Object.assign(obj, {
									id: userResponse.data.id,
								});
								Object.assign(obj, {
									firstname: userResponse.data.firstname,
								});
								Object.assign(obj, {
									lastname: userResponse.data.lastname,
								});
								Object.assign(obj, {
									profile: {
										roll_no:
											userResponse.data.profile.roll_no,
									},
								});
								Object.assign(obj, {
									image_original:
										userResponse.data.image_original,
								});
								return obj;
							});
						}
						setRole(userResponse.data.role);
						setResponseMsg("Logged in successfully!");
					} else {
						setResponseMsg(userResponse.data.message);
						setDialogVisible(true);
					}
				} else if (userResponse.status == "failed") {
					setResponseMsg(userResponse.message);
					setDialogVisible(true);
				}
				setIsLoggingIn(false);
			},
			signOut: async() => {
				setServer(null);
				setUserToken(null);
				await cache.remove("loginData");
				setResponseMsg("Logged out successfully!");
				setIsLoggingIn(false);
			},
		}),
		[]
	);


	const autoLogIn = async(server, username, password, is_Mobile) => {
		const userResponse = await handleLogin(
			server,
			username,
			password,
			is_Mobile
		);
		if (userResponse.status == 200) {
			if (!userResponse.data.error) {
				setServer(server);
				setUserToken(userResponse.data.access_token);
				setUserId(userResponse.data.id);
				setUsername(userResponse.data.firstname);
				const data = {
					user_id: userResponse.data.id,
					user_Token: userResponse.data.access_token
				}
				messaging().getToken().then(async (token) => {
					const response = await postDeviceId(server, token, data)
				})
				if (userResponse.data.role === "PARENT") {
					setStudents(userResponse.data.student);
					setActiveProfile(userResponse.data.student[0]);
				} else {
					setActiveProfile(function () {
						let obj = {};
						Object.assign(obj, {
							id: userResponse.data.id,
						});
						Object.assign(obj, {
							firstname: userResponse.data.firstname,
						});
						Object.assign(obj, {
							lastname: userResponse.data.lastname,
						});
						Object.assign(obj, {
							profile: {
								roll_no:
									userResponse.data.profile.roll_no,
							},
						});
						Object.assign(obj, {
							image_original:
								userResponse.data.image_original,
						});
						return obj;
					});
				}
				setRole(userResponse.data.role);
				setResponseMsg("Logged in successfully!");
			} else {
				setIsLoggingIn(false);		
			}
		} else if (userResponse.status == "failed") {
			setIsLoggingIn(false);		
		}
		setIsLoggingIn(false);	
	}


	useEffect(() => {
		setTimeout(async() => {
			const userCredential = await cache.get("loginData");
			if(userCredential && userCredential.server){
				let server = userCredential.server;
				let username = userCredential.username;
				let password = userCredential.password;
				autoLogIn(server, username,password, 1);
			}else {
				setIsLoggingIn(false);
			}
		}, 1000);
	}, []);

	const hideDialog = () => {
		setDialogVisible(false);
	};
	return (
		<AuthContext.Provider
			value={{
				authContext,
				isLoggingIn,
				responseMsg,
				server,
				userToken,
				userId,
				username,
				role,
				students,
				activeProfile,
				setActiveProfile,
				dialogVisible,
				hideDialog,
			}}
		>
			<NavigationContainer ref={navigationRef}>
				{userToken != null ? <AppNavigator initialRouteName={props.initialRouteName} /> : <AuthStack />}
			</NavigationContainer>
		</AuthContext.Provider>
	);
}
