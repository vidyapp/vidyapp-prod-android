import React, { useContext } from "react";
import { Platform } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { AuthContext } from "../components/Context";
import HomeStack from "./HomeStack";
import ProfileStack from "./ProfileStack";
import NewsStack from "./NewsStack";
import GradesStack from "./GradesStack";
import ClassesStack from "./ClassesStack";
import AssignmentsStack from "./AssignmentsStack";
import ResourcesStack from "./ResourcesStack";
import AttendanceStack from "./AttendanceStack";
import FinanceStack from "./FinanceStack";
import FormsStack from "./FormsStack";
import ContactStack from "./ContactStack";
import CustomDrawerContent from "../components/CustomDrawerContent";
import * as Colors from "../styles/colors";
import { saveDeviceID } from "../api/DataApi";
import CalendarStack from "./CalendarStack";

const Drawer = createDrawerNavigator();

export default function AppNavigator(props) {
	const { userToken, userId } = useContext(AuthContext);

	return (
		<Drawer.Navigator
			drawerContent={(props) => <CustomDrawerContent {...props} />}
			drawerStyle={{
				backgroundColor: Colors.lightBlueTheme,
			}}
			initialRouteName={props.initialRouteName}
		>
			<Drawer.Screen name="Home" component={HomeStack} />
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Profile"
				component={ProfileStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="News"
				component={NewsStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Calendar"
				component={CalendarStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Grades"
				component={GradesStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Classes"
				component={ClassesStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Assignments"
				component={AssignmentsStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Resources"
				component={ResourcesStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Attendance"
				component={AttendanceStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Finance"
				component={FinanceStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Forms"
				component={FormsStack}
			/>
			<Drawer.Screen
				options={{ unmountOnBlur: true }}
				name="Contact"
				component={ContactStack}
			/>
		</Drawer.Navigator>
	);
}
