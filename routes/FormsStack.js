import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { StyleSheet, Image } from "react-native";
import { bgmain } from "../img/index";
import FormsScreen from "../screens/FormsScreen";
import FormRequestScreen from "../screens/FormRequestScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";

const Stack = createStackNavigator();

export default function FormsStack() {
	return (
		<Stack.Navigator
			initialRouteName="Forms"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="Forms"
				component={FormsScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Forms"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
			<Stack.Screen
				name="FormRequest"
				component={FormRequestScreen}
				options={({ navigation, route }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title={route.params.title}
								titleColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
