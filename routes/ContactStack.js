import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ContactScreen from "../screens/ContactScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";
import { StyleSheet, Image } from "react-native";
import { bgmain } from "../img/index";

const Stack = createStackNavigator();

export default function ContactStack() {
	return (
		<Stack.Navigator
			initialRouteName="Contact"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="Contact"
				component={ContactScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Contact"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
