import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Avatar, Text } from "react-native-paper";
import { createStackNavigator } from "@react-navigation/stack";
import ProfileScreen from "../screens/ProfileScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";
import { profileBg } from "../img/index";
import { AuthContext } from "../components/Context";

const Stack = createStackNavigator();

export default function ProfileStack() {
	const { activeProfile } = React.useContext(AuthContext);
	let IMAGE_URL = { uri: activeProfile.image_original };
	return (
		<Stack.Navigator
			initialRouteName="Profile"
			screenOptions={{
				headerStyle: {
					backgroundColor: Colors.transparent,
					height: 268,
				},
			}}
		>
			<Stack.Screen
				name="Profile"
				component={ProfileScreen}
				options={({ navigation }) => {
					return {
						headerBackground: () => (
							<Image
								style={styles.backgroundImage}
								source={profileBg}
							></Image>
						),
						headerTitle: () => (
							<>
								<Header
									navigation={navigation}
									title="Profile"
									icon="menu"
									titleColor="#fff"
									iconColor="#fff"
								></Header>
								<View style={styles.head}>
									<Avatar.Image
										source={IMAGE_URL}
										size={72}
									/>
									<Text style={styles.name}>
										{activeProfile.firstname}{" "}
										{activeProfile.lastname}
									</Text>
									<Text style={styles.roll}>
										{activeProfile.roll_no}
									</Text>
								</View>
							</>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
	head: {
		alignItems: "center",
		justifyContent: "center",
	},
	name: {
		marginTop: 10,
		fontSize: 20,
		fontWeight: "bold",
		color: Colors.white,
	},
	roll: {
		fontSize: 14,
		color: Colors.white,
		marginBottom: 10,
	},
});
