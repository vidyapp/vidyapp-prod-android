import React from "react";
import { StyleSheet, Image } from "react-native";
import { bgmain } from "../img/index";
import { createStackNavigator } from "@react-navigation/stack";
import AttendanceScreen from "../screens/AttendanceScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";

const Stack = createStackNavigator();

export default function AttendanceStack() {
	return (
		<Stack.Navigator
			initialRouteName="Attendance"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="Attendance"
				component={AttendanceScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Attendance"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
