import React from "react";
import { StyleSheet, Image } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import AssignmentsSubjectsScreen from "../screens/AssignmentsSubjectsScreen";
import AssignmentsScreen from "../screens/AssignmentsScreen";
import AssignmentDetailScreen from "../screens/AssignmentDetailScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";
import { bgmain } from "../img/index";

const Stack = createStackNavigator();

export default function AssignmentsStack() {
	return (
		<Stack.Navigator
			initialRouteName="AssignmentsSubjects"
			screenOptions={() => {
				return {
					headerBackTitleVisible: false,
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="AssignmentsSubjects"
				component={AssignmentsSubjectsScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Assignments"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
			<Stack.Screen
				name="Assignments"
				component={AssignmentsScreen}
				options={({ navigation, route }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title={route.params.name}
								titleColor="#fff"
							/>
						),
					};
				}}
			/>
			<Stack.Screen
				name="AssignmentDetail"
				component={AssignmentDetailScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Assignment Detail"
								titleColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
