import React, { useState, useContext } from "react";
import { StyleSheet, Image, TouchableOpacity } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import DashboardScreen from "../screens/DashboardScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";
import { bgmain } from "../img/index";
import { Avatar } from "react-native-paper";
import { AuthContext, AccountContext } from "../components/Context";

const Stack = createStackNavigator();

export default function HomeStack() {
	const { activeProfile } = useContext(AuthContext);
	const [accountPickerVisible, setAccountPickerVisible] = useState(false);
	return (
		<AccountContext.Provider
			value={{ accountPickerVisible, setAccountPickerVisible }}
		>
			<Stack.Navigator
				initialRouteName="Dashboard"
				screenOptions={() => {
					return {
						headerBackground: () => (
							<Image
								style={styles.backgroundImage}
								source={bgmain}
							></Image>
						),
						headerStyle: {
							backgroundColor: Colors.blueTheme,
							height: 124,
						},
						headerTintColor: "white",
					};
				}}
			>
				<Stack.Screen
					name="Dashboard"
					component={DashboardScreen}
					options={({ navigation }) => {
						return {
							headerTitle: () => (
								<Header
									navigation={navigation}
									title="Dashboard"
									icon="menu"
									titleColor="#fff"
									iconColor="#fff"
									AccountSwitcher={
										<TouchableOpacity
											onPress={() =>
												setAccountPickerVisible(true)
											}
										>
											<Avatar.Image
												size={35}
												source={{
													uri:
														activeProfile.image_original,
												}}
											/>
										</TouchableOpacity>
									}
								/>
							),
						};
					}}
				/>
			</Stack.Navigator>
		</AccountContext.Provider>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
