import React from "react";
import {
	View,
	StyleSheet,
	ImageBackground,
	Image,
	TouchableOpacity,
} from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import GradesScreen from "../screens/GradesScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";
import { bgmain } from "../img/index";

const Stack = createStackNavigator();

export default function GradesStack() {
	return (
		<Stack.Navigator
			initialRouteName="Grades"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="Grades"
				component={GradesScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Grades"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
