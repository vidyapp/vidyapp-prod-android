import React from "react";
import { StyleSheet, Image } from "react-native";
import { bgmain } from "../img/index";
import { createStackNavigator } from "@react-navigation/stack";
import ClassesScreen from "../screens/ClassesScreen";
import ClassDetailScreen from "../screens/ClassDetailScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";

const Stack = createStackNavigator();

export default function ClassesStack() {
	return (
		<Stack.Navigator
			initialRouteName="Classes"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="Classes"
				component={ClassesScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Classes"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
			<Stack.Screen
				name="ClassDetail"
				component={ClassDetailScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Details"
								titleColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
