import React from "react";
import { StyleSheet, Image } from "react-native";
import { bgmain } from "../img/index";
import { createStackNavigator } from "@react-navigation/stack";
import ResourcesSubjectsScreen from "../screens/ResourcesSubjectsScreen";
import ResourcesScreen from "../screens/ResourcesScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";

const Stack = createStackNavigator();

export default function ResourcesStack() {
	return (
		<Stack.Navigator
			initialRouteName="ResourcesSubjects"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="ResourcesSubjects"
				component={ResourcesSubjectsScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Resources"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
			<Stack.Screen
				name="Resources"
				component={ResourcesScreen}
				options={({ navigation, route }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title={route.params.name}
								titleColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
