import React from "react";
import { StyleSheet, Image } from "react-native";
import { bgmain } from "../img/index";
import { createStackNavigator } from "@react-navigation/stack";
import FinanceScreen from "../screens/FinanceScreen";
import FinanceDetailScreen from "../screens/FinanceDetailScreen";
import Header from "../components/shared/Header";
import * as Colors from "../styles/colors";

const Stack = createStackNavigator();

export default function FinanceStack() {
	return (
		<Stack.Navigator
			initialRouteName="Finance"
			screenOptions={() => {
				return {
					headerBackground: () => (
						<Image
							style={styles.backgroundImage}
							source={bgmain}
						></Image>
					),
					headerStyle: {
						backgroundColor: Colors.blueTheme,
						height: 124,
					},
					headerTintColor: "white",
				};
			}}
		>
			<Stack.Screen
				name="Finance"
				component={FinanceScreen}
				options={({ navigation }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title="Finance"
								icon="menu"
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
			<Stack.Screen
				name="Detail"
				component={FinanceDetailScreen}
				options={({ navigation, route }) => {
					return {
						headerTitle: () => (
							<Header
								navigation={navigation}
								title={route.params.item.month}
								titleColor="#fff"
								iconColor="#fff"
							/>
						),
					};
				}}
			/>
		</Stack.Navigator>
	);
}
const styles = StyleSheet.create({
	backgroundImage: {
		position: "absolute",
		top: 0,
		width: "100%",
		height: "100%",
		resizeMode: "stretch",
		zIndex: -1,
	},
});
