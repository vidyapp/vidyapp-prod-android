import axios from "axios";

export const handleLogin = async (server, username, password, is_Mobile) => {
	try {
		const { data, status } = await axios({
			method: "post",
			url: `https://${server}/api/auth/login`,
			data: {
				username,
				password,
				is_Mobile,
			},
		});
		// console.log('login',data)
		return { data, status };
	} catch (error) {
		return { status: "failed", message: error.message };
	}
};
