import axios from "axios";

/*******************Post deviceId*************************/
export const postDeviceId = async (server, token, data) => {
	let formData = new FormData();
	formData.append('device_id', token); 
	formData.append('user_id', data.user_id); 
	formData.append('platform', 'ANDROID'); 

	const headers = { 
		Authorization: `Bearer ${data.user_Token}`, 
		Accept: 'multipart/form-data'
	 };
	try {
		const { data, status } = await axios({
			method: "post",
			url: `https://${server}/api/saveRegistrationID`,
			data: formData,			
			headers: headers,
		});
		return { data, status };
	} catch (error) {
		return { status: "failed", message: error.message };
	}
};

/********************GET USER PROFILE********************/
export const getUserProfile = async (server, token) => {
	const url = `https://${server}/api/getMyProfile`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "get",
			url: url,
			headers: headers,
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET PROFILE BY ID********************/
export const getProfileById = async (server, token, studentId) => {
	const url = `https://${server}/api/getMyProfileByID/${studentId}`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "get",
			url: url,
			headers: headers,
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************SEND MESSAGE********************/
export const sendMessage = async (server, token, student_id, subject, msg) => {
	const url = `https://${server}/api/sendMessage`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data, message },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				student_id,
				subject,
				message: msg,
			},
		});
		return { error, data, message, status };
	} catch (error) {
		return {
			error: true,
			message: "Message couldn't be sent",
			status: "failed",
		};
	}
};

/********************GET NEWS********************/
export const getNews = async (server, token, is_paginate) => {
	const url = `https://${server}/api/getNews`;
	const headers = { Authorization: `Bearer ${token}` };
	const is_mobile = 1;
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				is_paginate,
				is_mobile,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET ATTENDANCE********************/
export const getAttendance = async (server, token, year, month, student_id) => {
	const url = `https://${server}/api/getMyAttendance`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				year,
				month,
				student_id,
			},
		});
		return { error, status, data };
	} catch (error) {
		return {
			error: true,
			status: "failed",
			message: error.message,
		};
	}
};

/********************GET FORM REQUESTS********************/
export const getFormRequests = async (server, token, student_id, is_paginate) => {
	const url = `https://${server}/api/formRequest`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				student_id,
				is_paginate,
			},
		});
		return { error, status, data };
	} catch (error) {
		return {
			error: true,
			status: "failed",
			message: error.message,
		};
	}
};

/********************SEND FORM REQUESTS********************/
export const sendFormRequest = async (
	server,
	token,
	student_id,
	subject,
	msg,
	type,
	from_date,
	to_date
) => {
	const url = `https://${server}/api/sendFormRequest`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, message, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				student_id,
				subject,
				message: msg,
				type,
				from_date,
				to_date,
			},
		});

		return { error, data, message, status };
	} catch (error) {
		return {
			error: true,
			message: "Request couldn't be sent",
			status: "failed",
		};
	}
};

/********************GET BILLS********************/
export const getBills = async (server, token, year, student_id, is_paginate) => {
	const url = `https://${server}/api/getStudentBills`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				year,
				student_id,
				is_paginate,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET MONTHLY BILLS********************/
export const getMonthlyBills = async (server, token, bill_id, is_paginate) => {
	const url = `https://${server}/api/getStudentBillsDetails`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				bill_id,
				is_paginate,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET MY CLASSES********************/
export const getMyClasses = async (server, token, student_id) => {
	const url = `https://${server}/api/getMyClasses`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				student_id,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET CLASSES WITH ASSIGNMENT AND REOSURCES********************/
export const myAssignmentsAndResources = async (server, token, student_id) => {
	const url = `https://${server}/api/myAssignmentsResource`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: { student_id },
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET ASSIGNMENT AND REOSURCES FILES********************/
export const myAssignmentsAndResourcesFiles = async (
	server,
	token,
	subject_id,
	type
) => {
	const url = `https://${server}/api/myAssignmentsResourceFiles`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				subject_id,
				type,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************SUBMIT MY ASSIGNMENT********************/
export const submitMyAssignments = async (server, token, formData) => {
	const url = `https://${server}/api/submitMyAssignments`;
	try {
		axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
		// const options = {
		// 	onUploadProgress: (progressEvent) => {
		// 		const { loaded, total } = progressEvent;
		// 		let percent = Math.floor((loaded * 100) / total);
		// 		console.log(
		// 			`${(loaded / 1000000).toFixed(3)}MB of ${(
		// 				total / 1000000
		// 			).toFixed(3)}MB | ${percent}%`
		// 		);
		// 		//set progress state until progress iss less than 100%. set to 100 when we get
		// 		//response from server then after 1 sec make it disappear
		// 		//setProgress(percent);
		// 	},
		// };
		const {
			data: { data, error, message },
			status,
		} = await axios.post(url, formData);
		return { error, status, message, data };
	} catch (error) {
		console.log("Error", error);
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET MY GRADES********************/
export const getMyGrades = async (server, token, student_id) => {
	const url = `https://${server}/api/getMyGrades`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				student_id,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};

/********************GET EXAM LIST********************/
export const getExam = async (server, token, is_paginate) => {
	const url = `https://${server}/api/getExam`;
	const headers = { Authorization: `Bearer ${token}` };
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				is_paginate,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};


/********************GET CALENDAR********************/
export const getCalendar = async (server, token, year, month) => {
	const url = `https://${server}/api/getCalendar`;
	const headers = { Authorization: `Bearer ${token}` };
	const is_mobile = 1;
	try {
		const {
			data: { error, data },
			status,
		} = await axios({
			method: "post",
			url: url,
			headers: headers,
			data: {
				year,
				month,
				is_mobile,
			},
		});
		return { error, status, data };
	} catch (error) {
		return { error: true, status: "failed", message: error.message };
	}
};
