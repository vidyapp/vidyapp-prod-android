import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { Alert } from 'react-native';
import RootNavigator from "./routes/RootNavigator";
import SplashScreen from "react-native-splash-screen";
import Feather from 'react-native-vector-icons/Feather';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import * as RootNavigation from './routes/RootNavigation';

PushNotification.configure({
  onRegister: function (token) {
    // saveDeviceID(userToken, token.token, userId, Platform.OS);
    console.log("TOKEN:", token);
  },

  onNotification: function (notification) {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      // Alert.alert(notification.title, notification.message);
    });
    console.log("NOTIFICATION:", notification);

    // (required) Called when a remote is received or opened, or local notification is opened
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);
  },
  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  popInitialNotification: true,
  requestPermissions: true,
});

// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {

  console.log('Message handled in the background!', remoteMessage);
});

Feather.loadFont();

export default function App() {	
  const [initialRoute, setInitialRoute] = useState('Home');

	useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log(
        'FCM message received while app in the foreground:',
        remoteMessage,
      );
      // FCM message received while app in the foreground
      // Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
      Alert.alert(remoteMessage.notification.title, remoteMessage.notification.body,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK", onPress: () => {
            if (remoteMessage.data && remoteMessage.data.screen) {
              RootNavigation.navigate(remoteMessage.data.screen, {by_notification: true});
            }
          }
        }
      ]);
    });

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      if (remoteMessage.data && remoteMessage.data.screen) {
        RootNavigation.navigate(remoteMessage.data.screen, {by_notification: true});
      }
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          if (remoteMessage.data && remoteMessage.data.screen) {
            setInitialRoute(remoteMessage.data.screen); // e.g. "Settings"
          }
        }
      });
		SplashScreen.hide();		
	});
		
	return <RootNavigator initialRouteName={initialRoute} />;
}







