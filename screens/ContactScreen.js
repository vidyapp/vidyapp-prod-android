import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Keyboard } from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";

import { globalStyles } from "../styles/globalStyles";
import FlatButton from "../components/shared/FlatButton";
import { Colors } from "../styles";
import { sendMessage } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import DialogBox from "../components/shared/DialogBox";

export default function ContactScreen() {
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(false);
	const [responseMsg, setResponseMsg] = useState("");
	const [visible, setVisible] = useState(false);

	const handleMessage = async (token, subject, message, actions) => {
		setIsLoading(true);
		const userResponse = await sendMessage(
			server,
			token,
			activeProfile.id,
			subject,
			message
		);
		if (userResponse.status == 200) {
			setError(userResponse.error);
			setResponseMsg(userResponse.message);
			actions.resetForm();
		} else if (userResponse.status == "failed") {
			setError(userResponse.error);
			setResponseMsg(userResponse.message);
		}
		setIsLoading(false);
		setVisible(true);
	};

	const validationSchema = Yup.object().shape({
		subject: Yup.string().required().min(2).label("Subject"),
		message: Yup.string().required().min(5).label("Body"),
	});

	return (
		<View style={globalStyles.container}>
			<Formik
				initialValues={{ subject: "", message: "" }}
				validationSchema={validationSchema}
				onSubmit={({ subject, message }, actions) => {
					Keyboard.dismiss();
					handleMessage(userToken, subject, message, actions);
				}}
			>
				{({ handleChange, values, handleSubmit, errors, touched }) => (
					<View style={styles.content}>
						<View style={{ marginBottom: 5 }}>
							<TextInput
								style={styles.input}
								placeholder="Subject"
								onChangeText={handleChange("subject")}
								value={values.subject}
							/>
							<Text style={styles.errorText}>
								{touched.subject && errors.subject}
							</Text>
						</View>
						<View style={{ marginBottom: 5 }}>
							<TextInput
								style={styles.input}
								placeholder="Message Text"
								onChangeText={handleChange("message")}
								value={values.message}
								multiline
							/>
							<Text style={styles.errorText}>
								{touched.message && errors.message}
							</Text>
						</View>
						<FlatButton
							handleButtonPress={handleSubmit}
							buttonColor={Colors.teal}
							buttonLabel="Submit"
						/>
					</View>
				)}
			</Formik>
			{isLoading && <LoadingIndicator />}
			<DialogBox
				visible={visible}
				onDismiss={() => setVisible(false)}
				responseMsg={responseMsg}
				onPress={() => setVisible(false)}
			/>
		</View>
	);
}
const styles = StyleSheet.create({
	content: {
		paddingTop: 20,
	},
	inputLabel: {
		fontSize: 15,
		fontWeight: "bold",
		marginBottom: 10,
	},
	input: {
		borderRadius: 30,
		borderWidth: 1,
		borderColor: "#ccc",
		padding: 13,
		fontSize: 15,
		backgroundColor: "white",
	},
	errorText: {
		color: "crimson",
		marginBottom: 5,
		marginTop: 5,
	},
});
