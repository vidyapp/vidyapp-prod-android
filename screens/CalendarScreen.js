import React, { useState, useEffect } from "react";
import { View, ScrollView, StyleSheet, FlatList, Text, RefreshControl, SafeAreaView } from "react-native";
import { Calendar } from "react-native-calendars";
import { Colors } from "../styles";
import { globalStyles } from "../styles/globalStyles";
import { getCalendar } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import { ActivityIndicator } from "react-native-paper";
import Retry from "../components/shared/Retry";
import CalendarEventItem from "./../components/CalendarEventItem";

export default function CalendarScreen() {
	var months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken } = React.useContext(AuthContext);
	const [error, setError] = useState(false);
	const [msg, setMsg] = useState("");
	const [retry, setRetry] = useState(false);
	const [year, setYear] = useState(new Date().getFullYear());
	const [month, setMonth] = useState(months[new Date().getMonth()]);
	const [markedDates, setMarkedDates] = useState({});
	const [events, setEvents] = useState([]);

	const getAPI = async () => {
		setIsLoading(true);
		const {
			data: { levent },
			error,
			status,
			message,
		} = await getCalendar(server, userToken, year, month);
		if (status == 200) {
			setError(error);
			setEvents(levent);
			setMarkedDates(function () {
				let obj = {};
				for (let i = 0; i < levent.length; i++) {
					let data = {
						[levent[i].start_date]: {
							marked: true,
							dotColor: Colors.darkBlueTheme,
						},
					};
					Object.assign(obj, data);
				}
				return obj;
			});
		} else if (status == "failed") {
			setError(error);
			setMsg(message);
		}
		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [month, retry]);

	const handleDateChange = (date) => {
		setYear(date.year);
		setMonth(months[date.month - 1]);
	};
	return (
		<SafeAreaView style={globalStyles.container}>
			{!error ? (
				<ScrollView
							refreshControl={
			          <RefreshControl
			            refreshing={isLoading}
			            onRefresh={getAPI}
			          />
			        }>
					<View style={styles.calendar}>
						<Calendar
							onMonthChange={(date) => handleDateChange(date)}
							markedDates={markedDates}
						/>
					</View>
					{isLoading ? (
						<ActivityIndicator />
					) : (
						<View>
							{events.length === 0 ? (
								<Text style={{ textAlign: "center" }}>
									There are no events for this month
								</Text>
							) : (
								<FlatList
									data={events}
									keyExtractor={(item) => item.id.toString()}
									renderItem={({ item }) => (
										<CalendarEventItem calEvent={item} />
									)}
								/>
							)}
						</View>
					)}
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={msg}
				/>
			)}
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	calendar: {
		marginBottom: 15,
		borderRadius: 25,
		overflow: "hidden",
	},
});
