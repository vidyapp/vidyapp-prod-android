import React, { useState } from "react";
import DateTimePicker from "@react-native-community/datetimepicker";
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	TouchableOpacity,
	Keyboard,
} from "react-native";

import { Formik } from "formik";
import * as Yup from "yup";

import { globalStyles } from "../styles/globalStyles";
import FlatButton from "../components/shared/FlatButton";
import { Colors } from "../styles";
import { sendFormRequest } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import DialogBox from "../components/shared/DialogBox";

export default function FormRequestScreen({ route }) {
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(false);
	const [visible, setVisible] = useState(false);
	const [responseMsg, setResponseMsg] = useState("");
	const [source, setSource] = useState("");
	const [fromDate, setFromDate] = useState(
		new Date().getFullYear() +
			"-" +
			(new Date().getMonth() + 1) +
			"-" +
			new Date().getDate().toString()
	);
	const [toDate, setToDate] = useState(
		new Date().getFullYear() +
			"-" +
			(new Date().getMonth() + 1) +
			"-" +
			new Date().getDate().toString()
	);
	const [mode, setMode] = useState("date");
	const [show, setShow] = useState(false);

	const formatDate = (d) => {
		return (
			d.getFullYear() +
			"-" +
			(d.getMonth() + 1) +
			"-" +
			d.getDate().toString()
		);
	};
	const onDateChange = (event, selectedDate, source) => {
		setShow(Platform.OS === "ios");
		if (source == "from") {
			const currentDate = selectedDate || new Date();
			setFromDate(formatDate(currentDate));
		} else {
			const currentDate = selectedDate || new Date();
			setToDate(formatDate(currentDate));
		}
	};

	const showMode = (currentMode) => {
		setShow(true);
		setMode(currentMode);
	};

	const showDatePicker = (source) => {
		showMode("date");
		setSource(source);
	};

	const handleMessage = async (
		token,
		subject,
		message,
		type,
		fromDate,
		toDate,
		actions
	) => {
		setIsLoading(true);
		const userResponse = await sendFormRequest(
			server,
			token,
			activeProfile.id,
			subject,
			message,
			type,
			fromDate,
			toDate
		);
		if (userResponse.status == 200) {
			setError(userResponse.error);
			setResponseMsg(userResponse.message);
			actions.resetForm();
		} else if (userResponse.status == "failed") {
			setError(userResponse.error);
			setResponseMsg(userResponse.message);
		}
		setIsLoading(false);
		setVisible(true);
	};

	const validationSchema = Yup.object().shape({
		subject: Yup.string().required().min(2).label("Subject"),
		message: Yup.string().required().min(5).label("Body"),
	});

	return (
		<View style={globalStyles.container}>
			<Formik
				initialValues={{ subject: "", message: "" }}
				validationSchema={validationSchema}
				onSubmit={({ subject, message }, actions) => {
					Keyboard.dismiss();
					handleMessage(
						userToken,
						subject,
						message,
						route.params.type,
						fromDate,
						toDate,
						actions
					);
				}}
			>
				{({ handleChange, values, handleSubmit, errors, touched }) => (
					<View style={styles.content}>
						<View style={{ marginBottom: 5 }}>
							<TextInput
								style={styles.input}
								placeholder="Subject"
								onChangeText={handleChange("subject")}
								value={values.subject}
							/>
							<Text style={styles.errorText}>
								{touched.subject && errors.subject}
							</Text>
						</View>
						<View style={{ marginBottom: 5 }}>
							<TextInput
								style={styles.input}
								placeholder="Message Text"
								onChangeText={handleChange("message")}
								value={values.message}
								multiline
							/>
							<Text style={styles.errorText}>
								{touched.message && errors.message}
							</Text>
						</View>
						<View style={styles.datePicker}>
							<TouchableOpacity
								style={styles.fromDatePicker}
								onPress={() => showDatePicker("from")}
							>
								<Text>{fromDate}</Text>
							</TouchableOpacity>
							<View style={styles.separator}>
								<Text>to</Text>
							</View>
							<TouchableOpacity
								style={styles.toDatePicker}
								onPress={() => showDatePicker("to")}
							>
								<Text>{toDate}</Text>
							</TouchableOpacity>
						</View>
						<FlatButton
							handleButtonPress={handleSubmit}
							buttonColor={Colors.teal}
							buttonLabel="Submit"
						/>
					</View>
				)}
			</Formik>
			{show && (
				<DateTimePicker
					value={new Date()}
					onChange={(event, date) =>
						onDateChange(event, date, source)
					}
				/>
			)}
			{isLoading && <LoadingIndicator />}
			<DialogBox
				visible={visible}
				onDismiss={() => setVisible(false)}
				responseMsg={responseMsg}
				onPress={() => setVisible(false)}
			/>
		</View>
	);
}
const styles = StyleSheet.create({
	content: {
		paddingTop: 20,
	},
	inputLabel: {
		fontSize: 15,
		fontWeight: "bold",
		marginBottom: 10,
	},
	input: {
		borderRadius: 30,
		borderWidth: 1,
		borderColor: "#ccc",
		padding: 13,
		fontSize: 15,
		backgroundColor: "white",
	},
	datePicker: {
		flexDirection: "row",
		marginBottom: 20,
	},
	separator: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	fromDatePicker: {
		flex: 2,
		borderRadius: 30,
		borderWidth: 1,
		borderColor: "#ccc",
		padding: 13,
		fontSize: 15,
		backgroundColor: "white",
	},
	toDatePicker: {
		flex: 2,
		borderRadius: 30,
		borderWidth: 1,
		borderColor: "#ccc",
		padding: 13,
		fontSize: 15,
		backgroundColor: "white",
	},
	errorText: {
		color: "crimson",
		marginBottom: 5,
		marginTop: 5,
	},
});
