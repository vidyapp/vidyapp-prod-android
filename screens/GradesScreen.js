import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	StyleSheet,
	ImageBackground,
	TouchableOpacity,
	FlatList,
	ScrollView,
	SafeAreaView,
	RefreshControl,
} from "react-native";
import { globalStyles } from "../styles/globalStyles";
import { gpaBg } from "../img/index";
import { Colors, Typography } from "../styles";
import Card from "../components/shared/Cards";
import { getExam, getMyGrades } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";
import Message from "../components/shared/Message";

export default function GradesScreen() {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [examList, setExamList] = useState([]);
	const [gpa, setGpa] = useState("");
	const [grades, setGrades] = useState([]);
	const [status, setStatus] = useState("");
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	let grd;

	const getAPI = async () => {
		setIsLoading(true);
		const myGrades = await getMyGrades(server, userToken, activeProfile.id);
		const myExam = await getExam(server, userToken, 0);
		if (myGrades.status == 200 && myExam.status == 200) {
			setStatus(200);
			if (!myGrades.error && !myExam.error) {
				setError(false);
				setGpa(myGrades.data.gpa);
				setGrades(myGrades.data.grades);
				setExamList(myExam.data);
			} else {
				setError(true);
				setMessage("No data available");
			}
		} else if (myGrades.status == "failed" && myExam == "failed") {
			setStatus("failed");
			setError(true);
			setMessage(myGrades.message);
		}
		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [retry]);

	const [tab, setTab] = useState("grade");
	const handleTabChange = (name) => {
		setTab(name);
	};
	const getStyles = (pressed, target) => {
		if (target == "bg") {
			if (pressed === tab) return { backgroundColor: Colors.pink };
			else return { backgroundColor: Colors.white };
		} else {
			if (pressed === tab) return { color: Colors.white };
			else return { color: Colors.black };
		}
	};

	if (isLoading) {
		return <LoadingIndicator />;
	}
	return (
		<SafeAreaView style={globalStyles.container}>
			{status == 200 ? (
				<ScrollView
							refreshControl={
			          <RefreshControl
			            refreshing={isLoading}
			            onRefresh={getAPI}
			          />
			        }>
					{!error ? (
						<>
							<Card style={{ padding: 0 }}>
								<View style={{ flexDirection: "row" }}>
									<TouchableOpacity
										style={[
											styles.head,
											getStyles("grade", "bg"),
										]}
										onPress={() => handleTabChange("grade")}
									>
										<Text
											style={[
												styles.text,
												getStyles("grade", "font"),
											]}
										>
											Grades
										</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={[
											styles.head,
											getStyles("transcript", "bg"),
										]}
										onPress={() =>
											handleTabChange("transcript")
										}
									>
										<Text
											style={[
												styles.text,
												getStyles("transcript", "font"),
											]}
										>
											Transcript
										</Text>
									</TouchableOpacity>
								</View>
							</Card>
							<>
								{tab == "transcript" && (
									<View style={styles.card}>
										<ImageBackground
											source={gpaBg}
											style={styles.cardBgImage}
										>
											<View>
												<Text style={styles.caption}>
													Overall GPA
												</Text>
											</View>
											<View>
												<Text style={styles.titlelabel}>
													{gpa}
												</Text>
											</View>
										</ImageBackground>
									</View>
								)}

								<FlatList
									data={grades}
									ListHeaderComponent={() => (
										<View
											style={[
												styles.row,
												{
													backgroundColor:
														Colors.white,
												},
											]}
										>
											<View style={styles.subject}>
												<Text
													style={styles.tableHeader}
												>
													Subject
												</Text>
											</View>
											{examList.map((exam) => {
												return (
													<View
														key={exam.id.toString()}
														style={styles.exam_col}
													>
														<Text
															style={
																styles.tableHeader
															}
														>
															{exam.name}
														</Text>
													</View>
												);
											})}
										</View>
									)}
									stickyHeaderIndices={[0]}
									ListFooterComponent={() =>
										tab == "transcript" && (
											<View
												style={{
													marginTop: 10,
												}}
											>
												<Text
													style={{
														fontWeight: "bold",
														fontSize: 18,
													}}
												>
													GPA: {gpa}
												</Text>
											</View>
										)
									}
									keyExtractor={(item) => item.id.toString()}
									renderItem={({ item, index }) => {
										return (
											<View
												style={[
													styles.row,
													{
														backgroundColor:
															index % 2 == 0
																? Colors.grayTextBg
																: null,
													},
												]}
											>
												<View style={styles.subject}>
													<Text
														style={{
															fontWeight: "bold",
															color: "#555",
														}}
													>
														{item.name}
													</Text>
												</View>

												{examList.map((exam) => {
													let exist = item.grades.some(
														(element) => {
															if (
																exam.id ===
																element.exam.id
															)
																return (grd =
																	element.total_marks);
														}
													);

													return (
														<View
															key={exam.id.toString()}
															style={
																styles.exam_col
															}
														>
															<Text>
																{exist
																	? `${grd}`
																	: "NA"}
															</Text>
														</View>
													);
												})}
											</View>
										);
									}}
									contentContainerStyle={styles.cardContainer}
								/>
							</>
						</>
					) : (
						<Message message={message} />
					)}
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	head: {
		margin: 5,
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 5,
		borderRadius: 30,
		backgroundColor: Colors.pink,
	},
	text: {
		color: Colors.white,
		fontSize: 18,
	},
	headerText: {
		padding: 10,
		width: 152,
		height: 32,
		borderRadius: 20,
		fontSize: 15,
		fontWeight: "bold",
		textAlign: "center",
	},
	titlelabel: {
		fontSize: 25,
		fontWeight: "bold",
		lineHeight: 30,
		color: Colors.black,
	},
	card: {
		marginBottom: 30,
	},
	cardBgImage: {
		justifyContent: "center",
		paddingLeft: 20,
		height: 96,
		borderRadius: 25,
		overflow: "hidden",
	},
	cardContainer: {
		backgroundColor: "white",
		padding: 20,
		borderRadius: 20,
	},
	caption: {
		...Typography.regularSmallText,
		color: Colors.darkBlueTheme,
		lineHeight: 21,
	},
	tableHeader: {
		fontWeight: "bold",
		color: Colors.grayText,
	},
	row: {
		flexDirection: "row",
		borderRadius: 10,
		padding: 8,
		alignSelf: "stretch",
	},
	subject: {
		flex: 1.2,
		alignSelf: "stretch",
		marginRight: 10,
	},
	exam_col: {
		flex: 1,
	},
});
