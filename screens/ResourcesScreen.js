import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, FlatList, Linking } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors } from "../styles";
import { myAssignmentsAndResourcesFiles } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";
import Message from "../components/shared/Message";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function ResourcesScreen({ navigation, route }) {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken } = React.useContext(AuthContext);
	const [resources, setResources] = useState([]);
	const [status, setStatus] = useState("");
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	useEffect(() => {
		const getAPI = async () => {
			setIsLoading(true);
			const response = await myAssignmentsAndResourcesFiles(
				server,
				userToken,
				route.params.id,
				"RESOURCE"
			);
			if (response.status == 200) {
				setStatus(response.status);
				if (!response.error) {
					setError(response.error);
					setResources(response.data);
				} else {
					setError(response.error);
					setMessage("No data available");
				}
			} else if (response.status == "failed") {
				setStatus(response.status);
				setError(response.error);
				setMessage(response.message);
			}
			setIsLoading(false);
		};
		getAPI();
	}, [retry]);

	if (isLoading) {
		return <LoadingIndicator />;
	}
	return (
		<View style={globalStyles.container}>
			{status == 200 ? (
				<>
					{!error ? (
						<FlatList
							data={resources}
							keyExtractor={(item) => item.id.toString()}
							renderItem={({ item }) => (
								<Card>
									<View style={styles.head}>
										<Text style={styles.title}>
											{item.title}
										</Text>
									</View>
									{item.files.map((file) => {
										return (
											<TouchableOpacity
												style={styles.file}
												key={file.id.toString()}
												onPress={() =>
													Linking.openURL(
														`https://${server}/uploads/resourcec_assignment/${file.filename}`
													)
												}
											>
												<View style={styles.icon}>
													<Feather
														name="download"
														color={
															Colors.darkBlueTheme
														}
														size={30}
													/>
												</View>
												<Text
													numberOfLines={1}
													ellipsizeMode="middle"
													style={styles.fileName}
												>
													{file.filename}
												</Text>
											</TouchableOpacity>
										);
									})}
								</Card>
							)}
						/>
					) : (
						<Message message={message} />
					)}
				</>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</View>
	);
}
const styles = StyleSheet.create({
	head: {
		flexDirection: "row",
		alignItems: "center",
	},
	title: {
		flex: 1,
		fontSize: 18,
		marginBottom: 5,
	},
	file: {
		width: "70%",
		marginVertical: 10,
		flexDirection: "row",
		alignItems: "center",
		overflow: "hidden",
	},
	fileName: {
		flex: 1,
		fontSize: 15,
	},
	icon: {
		marginRight: 10,
	},
});
