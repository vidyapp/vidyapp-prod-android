import React, { useState } from "react";
import {
	ScrollView,
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
} from "react-native";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors } from "../styles";
import { bodyLabel } from "../styles/typography";

export default function ClassesScreen({ route }) {
	const formatTime = (time) => {
		const t = time.split(":");
		return t[0] + ":" + t[1];
	};

	return (
		<ScrollView style={globalStyles.container}>
			<Card style={{ padding: 0 }}>
				<View style={styles.card}>
					{route.params.start_time? 
						<View style={styles.time}>
							<Text style={styles.startTime}>
								{formatTime(route.params.start_time)}
							</Text>
							<Text style={styles.endTime}>
								{formatTime(route.params.end_time)}
							</Text>
						</View>
						:null
					}
					<View style={styles.class}>
						<Text style={styles.subject}>{route.params.name}</Text>
						<Text style={styles.caption}>
							{route.params.subject_code}
						</Text>
					</View>
				</View>
				<View style={{ padding: 20 }}>
					<View>
						<Text style={styles.title}>Lesson information</Text>
						{route.params.long_desc ? <Text>{route.params.long_desc}</Text>:null}
					</View>
					<View>
						<Text style={styles.title}>Resources</Text>
						<View>
							{route.params.resources.length != 0 &&
								route.params.resources.map((item, index) => (
									<Text key={item.id.toString()}>
										{`${index + 1}) `}
										{item.title}
									</Text>
								))}
						</View>
					</View>
					<View>
						<Text style={styles.title}>Assignment</Text>
						<View>
							{route.params.assignments.length != 0 &&
								route.params.assignments.map((item, index) => (
									<Text key={item.id.toString()}>
										{`${index + 1}) `}
										{item.title}
									</Text>
								))}
						</View>
					</View>
				</View>
			</Card>
		</ScrollView>
	);
}
const styles = StyleSheet.create({
	caption: {
		color: Colors.grayText,
	},
	card: {
		flexDirection: "row",
	},
	class: {
		flex: 8,
		padding: 20,
	},
	endTime: {
		fontWeight: "bold",
	},
	extras: {
		padding: 20,
	},
	extraText: {
		marginHorizontal: 10,
		marginVertical: 10,
		fontSize: 16,
	},
	icon: {
		flex: 2,
		alignItems: "center",
		justifyContent: "center",
	},
	startTime: {
		fontWeight: "bold",
		marginBottom: 15,
	},
	subject: {
		fontSize: 20,
		marginBottom: 5,
	},
	time: {
		flex: 2,
		borderRightWidth: 0.5,
		borderRightColor: "#aaa",
		alignItems: "center",
		padding: 20,
	},
	title: {
		...bodyLabel,
		marginVertical: 15,
	},
});
