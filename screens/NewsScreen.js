import React, { useState, useEffect } from "react";
import { View, Text, Image, StyleSheet, RefreshControl, ScrollView, SafeAreaView } from "react-native";

import HTML from "react-native-render-html";

import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors, Spacing, Typography } from "../styles";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { getNews } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";

export default function NewsScreen({ navigation, route }) {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken } = React.useContext(AuthContext);
	const [news, setNews] = useState([]);
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

  const getAPI = async () => {
  	setIsLoading(true);
  	const response = await getNews(server, userToken, 0);
  	if (response.status == 200) {
  		setError(response.error);
  		setNews(response.data);
  	} else if (response.status == "failed") {
  		setError(response.error);
  		setMessage(response.message);
  	}
  	setIsLoading(false);
  };

	useEffect(() => {
		getAPI();
	}, [retry, route]);
	if (isLoading) {
		return <LoadingIndicator />;
	}

	const labelColor = (type) => {
		return type == "NEWS" ? Colors.pink : Colors.lightBlueTheme;
	};
	return (
		<SafeAreaView style={globalStyles.container}>
			{!error ? (
				[news[0].id != null ? (
					<ScrollView
							refreshControl={
			          <RefreshControl
			            refreshing={isLoading}
			            onRefresh={getAPI}
			          />
			        }>
						<FlatList
							data={news}
							keyExtractor={(newsItem) => newsItem.id.toString()}
							renderItem={({ item }) => (
								<TouchableOpacity
									onPress={() =>
										navigation.navigate("NewsDetail", item)
									}
								>
									<Card>
										<View style={styles.newsHeader}>
											<View style={{ flexDirection: "row" }}>
												<Text style={styles.newsDate}>
													{item.timestamp}
												</Text>
												<View
													style={[
														styles.label,
														{
															backgroundColor: labelColor(
																item.type
															),
														},
													]}
												>
													<Text style={styles.labelText}>
														{item.type == "NEWS"
															? "News"
															: "Notice"}
													</Text>
												</View>
												<Image />
											</View>
											<Text style={styles.newsHead}>
												{item.title}
											</Text>
										</View>
										<View>
											<HTML html={item.short_desc} />

											<Text
												style={[
													styles.cardBodytext,
													styles.blueText,
												]}
											>
												Read More
											</Text>
										</View>
									</Card>
								</TouchableOpacity>
							)}
						/>
					</ScrollView>
				)
				:(
					<Text style={{ textAlign: "center" }}>There are no events for this month</Text>
				)]
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	newsHeader: {},
	cardLabel: {
		marginLeft: 150,
		marginBottom: 20,
		paddingVertical: 5,
		paddingHorizontal: 15,
		borderRadius: 20,
	},
	cardLabelText: {
		color: "white",
		fontWeight: "bold",
	},

	newsHead: {
		...Typography.bodyHeader,
		lineHeight: 30,
	},
	label: {
		borderRadius: 25,
		marginLeft: 10,
		paddingHorizontal: 10,
		paddingVertical: 3,
	},
	labelText: {
		color: Colors.white,
		fontSize: 15,
		fontWeight: "bold",
	},
	newsDate: {
		flex: 1,
		...Typography.lightsmallText,
		lineHeight: 18,
	},
	cardBodytext: {
		...Typography.regularSmallText,
		lineHeight: 21,
	},
	mgbot13: {
		paddingBottom: Spacing.small,
	},
	blueText: {
		color: Colors.darkBlueTheme,
	},
});
