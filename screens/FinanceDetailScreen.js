import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { globalStyles } from "../styles/globalStyles";
import * as Colors from "../styles/colors";
import Card from "../components/shared/Cards";
import { getMonthlyBills } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";

export default function FinanceDetailScreen({ route }) {
	const { server, userToken } = React.useContext(AuthContext);
	const [isLoading, setIsLoading] = useState(true);
	const [data, setData] = useState([]);
	const [dueBalance, setDueBalance] = useState("");
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	useEffect(() => {
		const getAPI = async () => {
			setIsLoading(true);
			const response = await getMonthlyBills(
				server,
				userToken,
				route.params.item.id,
				0
			);
			if (response.status == 200) {
				setError(response.error);
				setData(response.data.data);
				if (response.data.payment_status != "PAID") {
					setDueBalance(response.data.total);
				} else {
					setDueBalance(0);
				}
			} else if (response.status == "failed") {
				setError(response.error);
				setMessage(response.message);
			}
			setIsLoading(false);
		};
		getAPI();
	}, [retry]);
	if (isLoading) return <LoadingIndicator />;
	return (
		<View style={globalStyles.container}>
			{!error ? (
				<>
					<FlatList
						data={data}
						keyExtractor={(item) => item.id.toString()}
						renderItem={({ item }) => {
							return (
								<Card>
									<View style={{ flexDirection: "row" }}>
										<Text
											style={{
												flex: 1,
												color: Colors.grayText,
											}}
										>
											{item.title}
										</Text>
										<Text
											style={{ color: Colors.grayText }}
										>
											{item.month}
										</Text>
									</View>
									<View>
										<Text
											style={{
												fontWeight: "bold",
												fontSize: 18,
											}}
										>
											Rs. {item.amount}
										</Text>
									</View>
								</Card>
							);
						}}
					/>
					<Card style={{ flexDirection: "row" }}>
						<Text
							style={{
								flex: 1,
								fontSize: 18,
								fontWeight: "bold",
							}}
						>
							Total due for {`${route.params.item.month}`}
						</Text>
						<Text style={{ fontSize: 18, fontWeight: "bold" }}>
							Rs. {dueBalance}
						</Text>
					</Card>
				</>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</View>
	);
}
