import React, { useState } from "react";
import {
	View,
	Text,
	StyleSheet,
	Alert,
	ScrollView,
	FlatList,
	TouchableOpacity,
	Linking,
} from "react-native";
import { Divider } from "react-native-paper";
import Feather from "react-native-vector-icons/Feather";
import * as DocumentPicker from "expo-document-picker";
import { submitMyAssignments } from "../api/DataApi";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors } from "../styles";
import FloatingActionButton from "../components/FloatingActionButton";
import DialogBox from "../components/shared/DialogBox";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import { AuthContext } from "../components/Context";

export default function AssignmentDetailScreen({ route }) {
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [visible, setVisible] = useState(false);
	const [status, setStatus] = useState("");
	const [error, setError] = useState("");
	const [isUploading, setIsUploading] = useState(false);
	const [message, setMessage] = useState("");

	const uploadFile = async (server, userToken, formData) => {
		setIsUploading(true);
		const response = await submitMyAssignments(server, userToken, formData);
		setIsUploading(false);
		if (response.status == 200) {
			setStatus(response.status);
			if (!response.error) {
				setError(response.error);
				setMessage(response.message);
			} else {
				setError(response.error);
				setMessage(response.message.file[0]);
			}
		} else {
			setStatus(response.status);
			setMessage(response.message);
		}
		setVisible(true);
	};
	const openDocumentFile = async () => {
		try {
			const result = await DocumentPicker.getDocumentAsync({
				type: "application/*",
				copyToCacheDirectory: true,
			});
			if (result.type != "cancel") {
				let uriParts = result.uri.split(".");
				let fileType = uriParts[uriParts.length - 1];
				let formData = new FormData();
				formData.append("assignment_resource_id", route.params.id);
				formData.append("file", {
					uri: result.uri,
					name: result.name,
					type: `application/${fileType}`,
				});
				formData.append("student_id", activeProfile.id);
				Alert.alert(
					"Confirm",
					`Do you want to submit the file below?\n${result.name}`,
					[
						{
							text: "Cancel",
							onPress: () => console.log("Cancel pressed"),
						},
						{
							text: "Submit",
							onPress: () => uploadFile(server, userToken, formData),
						},
					],
					{ cancelable: false }
				);
			}
		} catch (err) {
			console.log(err);
		}
	};
	return (
		<View style={globalStyles.container}>
			<View
				style={{
					flex: 1,
					backgroundColor: "white",
					padding: 20,
					borderRadius: 25,
					overflow: "hidden",
				}}
			>
				<View style={{ marginBottom: 10 }}>
					<View style={styles.head}>
						<Text style={styles.title}>{route.params.title}</Text>

						{route.params.Is_active ? <Text>Active</Text> : null}
					</View>
					<Text style={styles.timestamp}>
						{route.params.timestamp}
					</Text>
				</View>
				<Divider />
				<View style={styles.description}>
					<Text style={styles.label}>Description</Text>
					<Text>{route.params.desc}</Text>
				</View>
				{ route.params.files.length > 0 ?
					<View style={styles.resource}>
						<Text style={styles.label}>Resources</Text>
						<ScrollView>
							{route.params.files.map((file) => {
								return (
									<TouchableOpacity
										style={styles.file}
										key={file.id.toString()}
										onPress={() =>
											Linking.openURL(
												`https://${server}/uploads/resourcec_assignment/${file.filename}`
											)
										}
									>
										<View style={styles.icon}>
											<Feather
												name="download"
												color={Colors.darkBlueTheme}
												size={30}
											/>
										</View>
										<Text
											numberOfLines={1}
											ellipsizeMode="middle"
											style={styles.fileName}
										>
											{file.filename}
										</Text>
									</TouchableOpacity>
								);
							})}
						</ScrollView>
					</View>
				: <></> }
				<DialogBox
					visible={visible}
					onDismiss={() => setVisible(false)}
					responseMsg={message}
					onPress={() => setVisible(false)}
				/>
			</View>

			<FloatingActionButton icon="upload" onPress={openDocumentFile} />
			{isUploading && <LoadingIndicator />}
		</View>
	);
}

const styles = StyleSheet.create({
	head: {
		flexDirection: "row",
		alignItems: "center",
	},
	title: {
		flex: 1,
		fontSize: 20,
		fontWeight: "bold",
		marginBottom: 5,
	},
	timestamp: {
		color: Colors.grayText,
	},
	label: {
		fontSize: 18,
		fontWeight: "bold",
	},
	description: {
		marginTop: 10,
	},
	resource: {
		marginBottom: 10,
	},
	file: {
		width: "70%",
		marginVertical: 10,
		flexDirection: "row",
		alignItems: "center",
	},
	fileName: {
		flex: 1,
		fontSize: 15,
	},
	icon: {
		marginRight: 10,
	},
});
