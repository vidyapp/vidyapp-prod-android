import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	TouchableOpacity,
	ScrollView,
	SafeAreaView,
	RefreshControl,
} from "react-native";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors } from "../styles";
import { myAssignmentsAndResourcesFiles } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";
import Message from "../components/shared/Message";

export default function AssignmentsScreen({ navigation, route }) {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken } = React.useContext(AuthContext);
	const [assignments, setAssignments] = useState([]);
	const [status, setStatus] = useState("");
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	const getAPI = async () => {
		setIsLoading(true);
		const response = await myAssignmentsAndResourcesFiles(
			server,
			userToken,
			route.params.id,
			"ASSIGNMENT"
		);
		console.log(response);
		if (response.status == 200) {
			setStatus(response.status);
			if (!response.error) {
				setError(response.error);
				setAssignments(response.data);
			} else {
				setError(response.error);
				setMessage("No data available");
			}
		} else if (response.status == "failed") {
			setStatus(response.status);
			setError(response.error);
			setMessage(response.message);
		}
		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [retry]);

	if (isLoading) {
		return <LoadingIndicator />;
	}
	return (
		<SafeAreaView style={globalStyles.container}>
			{status == 200 ? (
				<ScrollView
						refreshControl={
		          <RefreshControl
		            refreshing={isLoading}
		            onRefresh={getAPI}
		          />
		        }>
					{!error ? (
						<FlatList
							data={assignments}
							keyExtractor={(item) => item.id.toString()}
							renderItem={({ item }) => (
								<TouchableOpacity
									onPress={() =>
										navigation.navigate(
											"AssignmentDetail",
											item
										)
									}
								>
									<Card>
										<View style={styles.head}>
											<Text style={styles.title}>
												{item.title}
											</Text>

											{item.Is_active ? (
												<Text>Active</Text>
											) : null}
										</View>

										<Text style={styles.timestamp}>
											{item.timestamp}
										</Text>
									</Card>
								</TouchableOpacity>
							)}
						/>
					) : (
						<Message message={message} />
					)}
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	head: {
		flexDirection: "row",
		alignItems: "center",
	},
	title: {
		flex: 1,
		fontSize: 18,
		marginBottom: 5,
	},
	timestamp: {
		color: Colors.grayText,
	},
});
