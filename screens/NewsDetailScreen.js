import React, { useState } from "react";
import { View, Text, Image, StyleSheet, ScrollView, Linking, TouchableOpacity } from "react-native";
import { AuthContext } from "../components/Context";

import HTML from "react-native-render-html";

import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors, Spacing, Typography } from "../styles";
import ImageView from "react-native-image-viewing";

export default function NewsDetailScreen({ route }) {
	const { server } = React.useContext(AuthContext);
	const [visible, setIsVisible] = useState(false);
	return (
		<ScrollView style={globalStyles.container}>
			<Card>
				<View style={styles.newsHeader}>
					<Text style={styles.newsDate}>
						{route.params.timestamp}
					</Text>
					<Text style={styles.newsHead}>{route.params.title}</Text>
				</View>
				<View>
					<HTML
						style={[styles.cardBodytext, styles.mgbot13]}
						html={route.params.content || '<p></p>'}
						onLinkPress={(evt, href) => Linking.openURL(href) }
					/>
				</View>
				<TouchableOpacity onPress={() => setIsVisible(true) }>
					<Image
						style={styles.image}
						source={{
							uri: `https://${server}/uploads/news_notices/${route.params.file}`,
						}}
					/>
				</TouchableOpacity>
				<ImageView
				  images={[{
						uri: `https://${server}/uploads/news_notices/${route.params.file}`,
					}]}
				  imageIndex={0}
				  visible={visible}
				  onRequestClose={() => setIsVisible(false)}
				/>
			</Card>
		</ScrollView>
	);
}
const styles = StyleSheet.create({
	newsHeader: {},
	cardLabel: {
		marginLeft: 150,
		marginBottom: 20,
		paddingVertical: 5,
		paddingHorizontal: 15,
		borderRadius: 20,
	},
	cardLabelText: {
		color: "white",
		fontWeight: "bold",
	},
	newsLabel: {
		backgroundColor: Colors.pink,
	},
	newsHead: {
		...Typography.bodyHeader,
		lineHeight: 30,
	},
	noticeLabel: {
		backgroundColor: Colors.lightBlueTheme,
	},
	newsDate: {
		...Typography.lightsmallText,
		lineHeight: 18,
	},
	cardBodytext: {
		...Typography.regularSmallText,
		lineHeight: 21,
	},
	mgbot13: {
		paddingBottom: Spacing.small,
	},
	blueText: {
		color: Colors.darkBlueTheme,
	},
	image: {
		marginVertical: 20,
		borderRadius: 20,
		width: "100%",
		height: 200,
	},
});
