import React, { useState } from "react";
import {
	View,
	Text,
	TextInput,
	StyleSheet,
	Platform,
	StatusBar,
	ImageBackground,
	Keyboard,
	KeyboardAvoidingView,
	ScrollView,
	Pressable,
} from "react-native";
import { MaterialCommunityIcons } from "react-native-vector-icons/MaterialCommunityIcons";
import { Picker } from '@react-native-picker/picker';
import Modal from "react-native-modal";
import { Appbar } from "react-native-paper";
import { Formik } from "formik";
import * as Yup from "yup";
import { bgmain } from "../img/index";
import { TouchableOpacity } from "react-native-gesture-handler";
import FlatButton from "../components/shared/FlatButton";
import { Colors, Typography } from "../styles";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import DialogBox from "../components/shared/DialogBox";

export default function SignInScreen() {
	const {
		authContext: { signIn },
		responseMsg,
		isLoggingIn,
		dialogVisible,
		hideDialog,
	} = React.useContext(AuthContext);

	const [modalVisible, setModalVisible] = useState(false);
	const [passwordVisible, setPasswordVisible] = useState(false);

	const validationSchema = Yup.object().shape({
		username: Yup.string().required().label("Username"),
		password: Yup.string().required().min(4).label("Password"),
	});


	return (
		<View style={{ height: '100%' }}>
			<Modal isVisible={modalVisible}>
				<View style={styles.recoverResponseBox}>
					<View style={styles.recoverResponseView}>
						<Text style={{ fontSize: 16 }}>
							Please contact school to recover password
						</Text>
					</View>
					<View style={styles.recoverResponseBtn}>
						<FlatButton
							handleButtonPress={() => setModalVisible(false)}
							buttonColor={Colors.pink}
							buttonLabel="Ok"
						/>
					</View>
				</View>
			</Modal>
			<ImageBackground source={bgmain} style={styles.bgImage}>
				<StatusBar
					translucent={true}
					backgroundColor="rgba(0,0,0,0.2)"
					barStyle="light-content"
				/>
				<Appbar.Header style={styles.appHeader}>
					<Appbar.Content
						color={Colors.white}
						title="Login to your account"
						style={styles.contentTitle}
					/>
				</Appbar.Header>
			</ImageBackground>
			<KeyboardAvoidingView
			  keyboardVerticalOffset = {40} // adjust the value here if you need more padding
			  // style = {{ flex: 1 }}
			  behavior = "padding" >
			<ScrollView>
			<Formik
				initialValues={{ server: "app.vidyapp.co", username: "", password: "" }}
				validationSchema={validationSchema}
				onSubmit={({ server, username, password }) => {
					Keyboard.dismiss();
					signIn(server, username, password, 1);
				}}
			>
				{({ handleChange, values, handleSubmit, errors, touched }) => (
					<View style={styles.content}>
						<View style={{ marginBottom: 0 }}>
							<Text style={styles.inputLabel}>School</Text>
							<Picker
								onValueChange={handleChange("server")}
          			selectedValue={values.server}
							>
								<Picker.Item label="GEMS School" value="app.vidyapp.co" />
								<Picker.Item label="LFES School" value="lfes.vidyapp.co" />
			          <Picker.Item label="Geezerbuild Staging" value="staging.vidyapp.co" />
							</Picker>
							<Text style={styles.errorText}>
								{touched.server && errors.server}
							</Text>
						</View>
						<View style={{ marginBottom: 0 }}>
							<Text style={styles.inputLabel}>Username</Text>
							<TextInput
								style={styles.input}
								autoCapitalize="none"
								placeholder="Your Username"
								placeholderTextColor="#989BA2"
								onChangeText={handleChange("username")}
								value={values.username}
							/>
							<Text style={styles.errorText}>
								{touched.username && errors.username}
							</Text>
						</View>
						<View style={{ marginBottom: 0 }}>
							<Text style={styles.inputLabel}>Password</Text>
							<TextInput
								secureTextEntry={!passwordVisible}
								style={styles.input}
								placeholder="Your Password"
								placeholderTextColor="#989BA2"
			          autoCapitalize="none"
			          autoCorrect={false}
			          enablesReturnKeyAutomatically
								onChangeText={handleChange("password")}
								value={values.password}
							/>
							<TouchableOpacity
									onPress={() => setPasswordVisible(!passwordVisible)}>
								<Text
									style={styles.showPasswordBtn}
								>
									{passwordVisible ? 'Hide' : 'Show'} Password
								</Text>
							</TouchableOpacity>
							<Text style={styles.errorText}>
								{touched.password && errors.password}
							</Text>
						</View>
						<FlatButton
							handleButtonPress={handleSubmit}
							buttonColor={Colors.teal}
							buttonLabel="Log in"
							style={styles.loginBtn}
						/>
						<TouchableOpacity
								onPress={() => setModalVisible(true)}>
							<Text
								style={styles.recoverBtn}
							>
								Forgot Password?
							</Text>
						</TouchableOpacity>
					</View>
				)}
			</Formik>
			</ScrollView>
			</KeyboardAvoidingView>
			{isLoggingIn && <LoadingIndicator />}
			<DialogBox
				visible={dialogVisible}
				onDismiss={hideDialog}
				responseMsg={responseMsg}
				onPress={hideDialog}
			/>
		</View>
	);
}
const styles = StyleSheet.create({
	content: {
		height: "100%",
		textAlign: "center",
		padding: 20,
		backgroundColor: "transparent",
	},
	appHeader: {
		backgroundColor: "#FFFFFF00",
		height: 124,
	},
	contentTitle: {
		...Typography.screenHeader,
	},
	bgImage: {
		resizeMode: "cover",
		justifyContent: "center",
	},
	inputLabel: {
		...Typography.labelHead,
		marginBottom: 10,
	},
	input: {
		borderRadius: 24,
		borderWidth: 1,
		borderColor: Colors.lightGray,
		padding: 13,
		height: 44,
		backgroundColor: "white",
		...Typography.inputText,
	},
	responseMsgView: {
		marginBottom: 15,
		alignItems: "center",
	},
	responseMsg: {},
	loginBtn: {
		height: 44,
		backgroundColor: Colors.greenBlueTheme,
	},
	recoverBtn: {
		textAlign: "center",
		color: Colors.darkBlueTheme,
		...Typography.regularSmallText,
		marginTop: 20,
	},
	showPasswordBtn: {
		textAlign: "right",
		color: Colors.darkBlueTheme,
		...Typography.bodyText,
		marginTop: 5,
		marginRight: 10,
		marginBottom: 10,
	},
	recoverResponseBox: {
		backgroundColor: "white",
		borderRadius: 20,
	},
	recoverResponseView: {
		marginBottom: 20,
		padding: 20,
	},
	recoverResponseBtn: {
		width: "50%",
		alignSelf: "center",
	},
	errorText: {
		color: "crimson",
		marginBottom: 5,
		marginTop: 5,
	},
});
