import React, { useState, useEffect } from "react";
import { View, ScrollView, StyleSheet, SafeAreaView, RefreshControl } from "react-native";

import { Calendar } from "react-native-calendars";

import AttendanceListItem from "../components/AttendanceListItem";
import { Colors } from "../styles";
import { globalStyles } from "../styles/globalStyles";
import { getAttendance } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import { ActivityIndicator } from "react-native-paper";
import Retry from "../components/shared/Retry";

export default function AttendanceScreen() {
	var months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken, activeProfile } = React.useContext(AuthContext);

	const [year, setYear] = useState(new Date().getFullYear());
	const [month, setMonth] = useState(months[new Date().getMonth()]);
	const [totalDays, setTotalDays] = useState("");
	const [presentDays, setPresentDays] = useState("");
	const [absentDays, setAbsentDays] = useState("");
	const [markedDates, setMarkedDates] = useState({});
	const [err, setErr] = useState(false);
	const [msg, setMsg] = useState("");
	const [retry, setRetry] = useState(false);

	const getAPI = async () => {
		setIsLoading(true);
		const {
			data: { attendance, total },
			error,
			status,
			message,
		} = await getAttendance(server, userToken, year, month, activeProfile.id);
		if (status == 200) {
			setErr(error);
			setTotalDays(total);
			const present = attendance.filter((item) => {
				if (item.present === "Present") return true;
				else return false;
			}).length;
			setPresentDays(present);
			const absent = attendance.filter((item) => {
				if (item.present === "Absent") return true;
				else return false;
			}).length;
			setAbsentDays(absent);
			setMarkedDates(function () {
				let obj = {};
				for (let i = 0; i < attendance.length; i++) {
					let data = {
						[attendance[i].date]: {
							marked: true,
							dotColor:
								attendance[i].present == "Present"
									? Colors.green
									: Colors.pink,
						},
					};
					Object.assign(obj, data);
				}
				return obj;
			});
		} else if (status == "failed") {
			setErr(error);
			setMsg(message);
		}

		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [month, retry]);

	const handleDateChange = (date) => {
		setYear(date.year);
		setMonth(months[date.month - 1]);
	};
	return (
		<SafeAreaView style={globalStyles.container}>
			{!err ? (
				<ScrollView
						refreshControl={
		          <RefreshControl
		            refreshing={isLoading}
		            onRefresh={getAPI}
		          />
		        }>
					<View style={styles.calendar}>
						<Calendar
							onMonthChange={(month) =>
								handleDateChange(month, userToken)
							}
							markedDates={markedDates}
						/>
					</View>
					{isLoading ? (
						<ActivityIndicator />
					) : (
						<ScrollView>
							<AttendanceListItem
								label="Total Present"
								number={presentDays}
								backgroundColor={Colors.green}
							/>
							<AttendanceListItem
								label="Total Absent"
								number={absentDays}
								backgroundColor={Colors.pink}
							/>
							<AttendanceListItem
								label="Total Working Days"
								number={totalDays}
								backgroundColor={Colors.darkBlueTheme}
							/>
						</ScrollView>
					)}
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={msg}
				/>
			)}
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	calendar: {
		marginBottom: 15,
		borderRadius: 25,
		overflow: "hidden",
	},
});
