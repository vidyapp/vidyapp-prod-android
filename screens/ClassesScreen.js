import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	LayoutAnimation,
	Platform,
	UIManager,
	TouchableOpacity,
	ScrollView,
	SafeAreaView,
	RefreshControl,
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors } from "../styles";
import { getMyClasses } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";

export default function ClassesScreen({ navigation }) {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [classes, setClasses] = useState([]);
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	const getAPI = async () => {
		setIsLoading(true);
		const response = await getMyClasses(server, userToken, activeProfile.id);
		if (response.status == 200) {
			setError(response.error);
			setClasses(response.data.subjects);
		} else if (response.status == "failed") {
			setError(response.error);
			setMessage(response.message);
		}
		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [retry]);

	const [expanded, setExpanded] = useState({});
	const [icon, setIcon] = useState({});

	useEffect(() => {
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
	}, []);

	const handleShortDetail = (item) => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		if (Object.keys(expanded).length == 0) {
			setExpanded({ [item]: true });
		} else {
			setExpanded((prevState) => ({
				...prevState,
				[item]: !prevState[item],
			}));
		}
		expanded[item]
			? setIcon({ ...icon, [item]: "chevron-down" })
			: setIcon({ ...icon, [item]: "chevron-up" });
	};

	const formatTime = (time) => {
		console.log('time',time)
		const t = time.split(":");
		return t[0] + ":" + t[1];
	};

	if (isLoading) {
		return <LoadingIndicator />;
	}
	return (
		<SafeAreaView style={globalStyles.container}>
			{!error ? (
				<ScrollView
						refreshControl={
		          <RefreshControl
		            refreshing={isLoading}
		            onRefresh={getAPI}
		          />
		        }>
					<FlatList
						data={classes}
						keyExtractor={(item) => item.id.toString()}
						renderItem={({ item }) => (
							<TouchableOpacity
								onPress={() =>
									navigation.navigate("ClassDetail", item)
								}
							>
								<Card style={{ padding: 0 }}>
									<View style={styles.card}>
										<View style={styles.time}>
											{item.start_time?<Text style={styles.startTime}>
												{formatTime(item.start_time)}
												</Text>: null }
											{item.start_time?<Text style={styles.endTime}>
												{formatTime(item.end_time)}
												</Text>: null }
										</View>
										<View style={styles.class}>
											<Text style={styles.subject}>
												{item.name}
											</Text>
											<Text style={styles.caption}>
												{item.subject_code}
											</Text>
										</View>
										<TouchableOpacity
											style={styles.icon}
											onPress={() =>
												handleShortDetail(item.id)
											}
										>
											<Entypo
												name={
													icon[item.id] || "chevron-down"
												}
												size={20}
											/>
										</TouchableOpacity>
									</View>
									<View
										style={{
											height:
												expanded[item.id] || false
													? null
													: 0,
											overflow: "hidden",
										}}
									>
										<View style={styles.extras}>
											{/* <Text
												style={[
													styles.extraText,
													{
														color: Colors.grayText,
														fontSize: null,
													},
												]}
											>
												Classroon 4A/ West Building
											</Text> */}
											<Text style={styles.extraText}>
												Teacher:{" "}
												{item.subject_teacher.firstname}{" "}
												{item.subject_teacher.lastname}
											</Text>
											<Text style={styles.extraText}>
												{item.short_desc}
											</Text>
										</View>
									</View>
								</Card>
							</TouchableOpacity>
						)}
					/>
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	card: {
		flexDirection: "row",
	},
	class: {
		flex: 8,
		padding: 20,
	},
	time: {
		flex: 2,
		borderRightWidth: 0.5,
		borderRightColor: "#aaa",
		alignItems: "center",
		padding: 20,
	},
	startTime: {
		fontWeight: "bold",
		marginBottom: 15,
	},
	endTime: {
		fontWeight: "bold",
	},
	subject: {
		fontSize: 20,
		marginBottom: 5,
	},
	caption: {
		color: Colors.grayText,
	},
	icon: {
		flex: 2,
		alignItems: "center",
		justifyContent: "center",
	},
	extras: {
		padding: 20,
	},
	extraText: {
		marginHorizontal: 10,
		marginVertical: 10,
		fontSize: 16,
	},
});
