import React, { useEffect, useState } from "react";
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	TouchableOpacity,
	ScrollView,
	SafeAreaView,
	RefreshControl,
} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { globalStyles } from "../styles/globalStyles";
import * as Colors from "../styles/colors";
import Card from "../components/shared/Cards";
import { getBills } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import { ActivityIndicator } from "react-native-paper";
import Retry from "../components/shared/Retry";

export default function FinanceScreen({ navigation }) {
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [isLoading, setIsLoading] = useState(true);
	const [year, setYear] = useState(new Date().getFullYear());
	const [data, setData] = useState([]);
	const [dueBalance, setDueBalance] = useState("");
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	const getIcon = (value) => {
		return value <= 0 ? "check" : "alert-circle";
	};
	const getBadgeColor = (value) => {
		return value <= 0 ? Colors.teal : Colors.pink;
	};

	const getAPI = async () => {
		setIsLoading(true);
		const response = await getBills(
			server,
			userToken,
			year,
			activeProfile.id,
			0
		);
		if (response.status == 200) {
			if (!response.error) {
				setError(response.error);
				setData(response.data);
				const balance = response.data.filter((item) => {
					if (item.status == "PENDING") {
						return true;
					}
				});
				const totalBalance = balance.map((item) =>
					parseInt(item.amount)
				);
				setDueBalance(
					totalBalance.reduce((acc, cur) => {
						return acc + cur;
					}, 0)
				);
			} else {
				setError(false);
				setData(response.data);
				setMessage("No data available for current year");
			}
		} else if (response.status == "failed") {
			setError(response.error);
			setMessage(response.message);
		}
		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [year, retry]);

	return (
		<SafeAreaView style={globalStyles.container}>
			<ScrollView
					refreshControl={
	          <RefreshControl
	            refreshing={isLoading}
	            onRefresh={getAPI}
	          />
	        }>
				<Card>
					<View style={{ flexDirection: "row" }}>
						<View
							style={[
								styles.badge,
								{
									backgroundColor: getBadgeColor(dueBalance),
								},
							]}
						>
							<Feather
								name={getIcon(dueBalance)}
								size={25}
								color="white"
							/>
						</View>
						<View>
							<Text style={styles.balanceLabel}>Balance Due</Text>
							<View style={styles.balance}>
								<Text>Total: </Text>
								<Text>Rs. {dueBalance}</Text>
							</View>
						</View>
					</View>
				</Card>
				<Card style={{ flexDirection: "row", paddingVertical: 13 }}>
					<TouchableOpacity
						onPress={() => setYear((prevYear) => prevYear - 1)}
					>
						<Feather
							name="chevron-left"
							size={30}
							color={Colors.darkBlueTheme}
						/>
					</TouchableOpacity>
					<Text style={styles.year}>{year}</Text>
					<TouchableOpacity
						onPress={() => setYear((prevYear) => prevYear + 1)}
					>
						<Feather
							name="chevron-right"
							size={30}
							color={Colors.darkBlueTheme}
						/>
					</TouchableOpacity>
				</Card>
				{!error ? (
					<>
						<Card>
							<View style={styles.table}>
								<View style={styles.row}>
									<View style={styles.cell}>
										<Text style={styles.tableHeader}>Time</Text>
									</View>
									<View style={styles.cell}>
										<Text style={styles.tableHeader}>
											Amount
										</Text>
									</View>
									<View style={styles.cell}>
										<Text style={styles.tableHeader}>
											Status
										</Text>
									</View>
								</View>
							</View>
							{!isLoading ? (
								<>
									{data.length > 0 ? (
										<FlatList
											data={data}
											keyExtractor={(item) =>
												item.id.toString()
											}
											renderItem={({ item, index }) => {
												return (
													<TouchableOpacity
														style={[
															styles.row,
															{
																backgroundColor:
																	index % 2 == 0
																		? Colors.grayTextBg
																		: null,
															},
														]}
														onPress={() => {
															navigation.navigate(
																"Detail",
																{
																	item,
																}
															);
														}}
													>
														<View style={styles.cell}>
															<Text>
																{item.month}
															</Text>
														</View>
														<View style={styles.cell}>
															<Text>
																Rs. {item.amount}
															</Text>
														</View>
														<View style={styles.cell}>
															<Text
																style={{
																	textTransform:
																		"capitalize",
																}}
															>
																{item.status}
															</Text>
														</View>
													</TouchableOpacity>
												);
											}}
										/>
									) : (
										<View
											style={{
												alignItems: "center",
												justifyContent: "center",
											}}
										>
											<Text>{message}</Text>
										</View>
									)}
								</>
							) : (
								<ActivityIndicator />
							)}
						</Card>
					</>
				) : (
					<Retry
						handleRetry={() => setRetry((prevState) => !prevState)}
						message={message}
					/>
				)}
			</ScrollView>
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	head: { height: 40, backgroundColor: "#f1f8ff" },
	text: { margin: 6 },
	badge: {
		paddingHorizontal: 15,
		paddingVertical: 15,
		borderRadius: 30,
		marginRight: 20,
	},
	balanceLabel: {
		fontWeight: "bold",
		marginRight: 20,
	},
	balance: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	year: {
		flex: 1,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
	table: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		marginBottom: 30,
	},
	tableHeader: {
		fontWeight: "bold",
		color: Colors.grayText,
	},
	row: {
		flexDirection: "row",
		borderRadius: 10,
		padding: 8,
		alignSelf: "stretch",
	},
	cell: { flex: 1, alignSelf: "stretch" },
});
