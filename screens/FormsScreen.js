import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	TouchableOpacity,
} from "react-native";
import Modal from "react-native-modal";
import Feather from "react-native-vector-icons/Feather";
import { globalStyles } from "../styles/globalStyles";
import FormListItem from "../components/FormListItem";
import FloatingActionButton from "../components/FloatingActionButton";
import { getFormRequests } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Message from "../components/shared/Message";
import Retry from "../components/shared/Retry";
import { Colors } from "../styles";

export default function FormsScreen({ navigation, route }) {
	const [buttonIcon, setButtonIcon] = useState("plus");
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [userData, setUserData] = useState([]);
	const [status, setStatus] = useState("");
	const [refreshing, setRefreshing] = useState(false);
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [reload, setReload] = useState(true);
	const [retry, setRetry] = useState(false);
	useEffect(() => {
		const getAPI = async () => {
			setIsLoading(true);
			const response = await getFormRequests(
				server,
				userToken,
				activeProfile.id,
				0
			);
			if (response.status == 200) {
				setStatus(response.status);
				if (!response.error) {
					setError(response.error);
					setUserData(response.data);
				} else {
					setError(response.error);
					setMessage("No data available");
				}
			} else if (response.status == "failed") {
				setStatus(response.status);
				setError(response.error);
				setMessage(response.message);
			}
			setIsLoading(false);
		};
		getAPI();
	}, [reload, retry, route]);

	const [modalVisible, setModalVisible] = useState(false);

	if (isLoading) {
		return <LoadingIndicator />;
	}

	const handleButtonPress = () => {
		setModalVisible(!modalVisible);
		setButtonIcon("x");
	};

	return (
		<>
			<View style={globalStyles.container}>
				{status == 200 ? (
					<>
						{!error ? (
							<FlatList
								data={userData}
								keyExtractor={(item) => item.id.toString()}
								renderItem={({ item }) => (
									<FormListItem
										status={item.form_status}
										title={item.subject}
										description={item.message}
										from={item.from_date}
										to={item.to_date}
									/>
								)}
								refreshing={refreshing}
								onRefresh={() => {
									setReload((prevState) => !prevState);
								}}
							/>
						) : (
							<Message message={message} />
						)}
						<Modal
							isVisible={modalVisible}
							onBackdropPress={() => {
								setModalVisible(!modalVisible);
								setButtonIcon("plus");
							}}
						>
							<View style={styles.modalView}>
								<TouchableOpacity
									style={styles.req}
									onPress={() => {
										navigation.navigate("FormRequest", {
											title: "Leave Request Form",
											type: "LEAVE",
										});
										setModalVisible(false);
										setButtonIcon("plus");
									}}
								>
									<Feather
										name="file-text"
										size={26}
										color={Colors.iconBlue}
										backgroundColor={Colors.iconBgBlue}
									/>

									<Text style={styles.text}>
										Leave Request Form
									</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={styles.req}
									onPress={() => {
										navigation.navigate("FormRequest", {
											title: "Absentee Request Form",
											type: "ABSENTEE",
										});
										setModalVisible(false);
										setButtonIcon("plus");
									}}
								>
									<Feather
										name="file-text"
										size={26}
										color={Colors.iconPurple}
										backgroundColor={Colors.iconBgPurple}
									/>
									<Text style={styles.text}>
										Absentee Form
									</Text>
								</TouchableOpacity>
							</View>
						</Modal>
						<FloatingActionButton
							icon={buttonIcon}
							onPress={handleButtonPress}
						/>
					</>
				) : (
					<Retry
						handleRetry={() => setRetry((prevState) => !prevState)}
						message={message}
					/>
				)}
			</View>
		</>
	);
}
const styles = StyleSheet.create({
	cardLabel: {
		padding: 5,
		backgroundColor: "gray",
		borderRadius: 10,
	},
	cardLabelText: {
		color: "white",
	},
	titleText: {
		fontSize: 20,
		fontWeight: "bold",
	},
	modalView: {
		position: "absolute",
		bottom: 70,
		right: 10,
		paddingVertical: 10,
		alignSelf: "center",
		backgroundColor: "white",
		borderRadius: 20,
	},
	req: {
		padding: 10,
		marginHorizontal: 15,
		flexDirection: "row",
		alignItems: "center",
	},
	text: {
		marginHorizontal: 10,
		fontSize: 15,
	},
});
