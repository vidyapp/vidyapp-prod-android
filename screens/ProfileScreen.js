import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Image, ScrollView } from "react-native";
import { Button, Divider } from "react-native-paper";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors, Spacing, Typography } from "../styles";
import { emergencyLogo } from "../img/index";
import { getProfileById } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";

export default function ProfileScreen() {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [userData, setUserData] = useState([]);
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);
	useEffect(() => {
		const getAPI = async () => {
			setIsLoading(true);
			const response = await getProfileById(server, userToken, activeProfile.id);
			if (response.status == 200) {
				setError(response.error);
				setUserData(response.data);
			} else if (response.status == "failed") {
				setError(response.error);
				setMessage(response.message);
			}
			setIsLoading(false);
		};
		getAPI();
	}, [retry]);
	if (isLoading) {
		return <LoadingIndicator />;
	}
	return (
		<View style={globalStyles.container}>
			{!error ? (
				<ScrollView showsVerticalScrollIndicator={false}>
					<Card style={styles.profileSection}>
						<Text style={styles.cardTitle}>General</Text>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>
								Date of Birth
							</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.dob}
							</Text>
							<Divider style={Colors.dividercolor} />
						</View>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>
								Class / Section
							</Text>
							<Text style={styles.profileInfo}>
								{userData[0].class_id.class_name} /{" "}
								{userData[0].class_id.class_section}
							</Text>
							<Divider style={Colors.dividercolor} />
						</View>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>Roll number</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.roll_no}
							</Text>
							<Divider style={Colors.dividercolor} />
						</View>
						{
							(userData[0].profile.day_hostel) ?
								<View style={styles.profileDetailrow}>
									<Text style={styles.profileLabel}>
										Day Scholar / Residental
									</Text>
									<Text style={styles.profileInfo}>
										{userData[0].profile.day_hostel}
									</Text>
									<Divider style={Colors.dividercolor} />
								</View>
							: null
						}
						{
							(userData[0].profile.transportation) ?
								<View style={styles.profileDetailrow}>
									<Text style={styles.profileLabel}>
										Transportation
									</Text>
									<Text style={styles.profileInfo}>
										{userData[0].profile.transportation}
									</Text>
									{
										(userData[0].profile.transportation == 'Bus User') ?
											<Text style={styles.profileInfo}>
												Bus Number: {userData[0].profile.bus_no}
											</Text>
										: null
									}
									<Divider style={Colors.dividercolor} />
								</View>
							: null
						}
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>
								Phone Number
							</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.phone}
							</Text>
							<Divider style={Colors.dividercolor} />
						</View>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>Landline</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.landline}
							</Text>
						</View>
					</Card>
					<Card style={styles.profileSection}>
						<Text style={styles.cardTitle}>Parent Detail</Text>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>
								{userData[0].profile.parent_a_name}
							</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.parent_a_contact}
							</Text>
							<Divider style={Colors.dividercolor} />
						</View>
						{
							(userData[0].profile.parent_b_name || userData[0].profile.parent_b_contact) ?
								<View style={styles.profileDetailrow}>
									<Text style={styles.profileLabel}>
										{userData[0].profile.parent_b_name}
									</Text>
									<Text style={styles.profileInfo}>
										{userData[0].profile.parent_b_contact}
									</Text>
								</View>
							: null
						}
					</Card>
					<Card style={styles.profileSection}>
						<Text style={styles.cardTitle}>Medical Condition</Text>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>
								Disabilities
							</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.disabilities}
							</Text>
							<Divider style={Colors.dividercolor} />
						</View>
						<View style={styles.profileDetailrow}>
							<Text style={styles.profileLabel}>Allergies</Text>
							<Text style={styles.profileInfo}>
								{userData[0].profile.allergies}
							</Text>
						</View>
					</Card>
					<Card style={styles.profileSection}>
						<View style={styles.contactSection}>
							<View>
								<Image
									style={styles.eLogo}
									source={emergencyLogo}
								></Image>
							</View>
							<View style={styles.pdLeftbase}>
								<Text style={styles.cardTitle}>
									Emergency Contacts
								</Text>

								<View style={styles.profileDetailrow}>
									<Text style={styles.contactNum}>
										{userData[0].profile.emergency_contact}
									</Text>
								</View>
							</View>
						</View>
					</Card>
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</View>
	);
}
const styles = StyleSheet.create({
	profileSection: {
		paddingBottom: Spacing.base,
	},
	cardTitle: {
		...Typography.bodyHeader,
		paddingBottom: Spacing.base,
	},
	profileDetailrow: {
		paddingBottom: Spacing.smaller,
	},
	profileLabel: {
		...Typography.bodyLabel,
		paddingBottom: Spacing.tiny,
		lineHeight: 24,
	},
	profileInfo: {
		...Typography.bodyText,
		paddingBottom: Spacing.small,
		lineHeight: 21,
	},
	contactInfo: {
		...Typography.bodyText,
		lineHeight: 21,
	},
	contactNum: {
		paddingBottom: Spacing.small,
		lineHeight: 24,
		color: Colors.lightBlueTheme,
	},
	eLogo: {
		width: 52,
		height: 52,
	},
	contactSection: {
		flex: 1,
		flexDirection: "row",
	},
	pdLeftbase: {
		paddingLeft: Spacing.base,
	},
});
