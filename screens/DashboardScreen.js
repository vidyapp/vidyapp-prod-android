import React from "react";
import {
	View,
	Text,
	StyleSheet,
	Image,
	ImageBackground,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight,
	FlatList,
} from "react-native";
import Modal from "react-native-modal";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { gemsLogo, lfesLogo, eventsBg, newsBg, gpaBg } from "../img/index";
import { Colors, Typography } from "../styles";
import { AuthContext, AccountContext } from "../components/Context";
import { Avatar } from "react-native-paper";

export default function DashboardScreen({ navigation }) {
	const {
		username,
		activeProfile,
		setActiveProfile,
		students,
		server,
	} = React.useContext(AuthContext);
	const { accountPickerVisible, setAccountPickerVisible } = React.useContext(
		AccountContext
	);
	const focusActiveProfile = (id) => {
		if (id == activeProfile.id) {
			return { backgroundColor: "#eee" };
		}
	};
	let schoolLogoStyle = styles.gemsLogo;
	let schoolLogo = gemsLogo;
	switch(server) {
		case "staging.vidyapp.co":
			schoolLogoStyle = styles.gemsLogo;
			schoolLogo = gemsLogo;
			break;
		case "app.vidyapp.co":
			schoolLogoStyle = styles.gemsLogo;
			schoolLogo = gemsLogo;
			break;
		case "lfes.vidyapp.co":
			schoolLogoStyle = styles.lfesLogo;
			schoolLogo = lfesLogo;
			break;
	}
	return (
		<View style={globalStyles.container}>
			<ScrollView showsVerticalScrollIndicator={false}>
				<Card style={styles.whitebg}>
					<View style={styles.flexrowSection}>
						<View style={styles.messageflexSection}>
							<View>
								<Text style={styles.titleMessage}>
									Have a great day,
								</Text>
							</View>
							<View>
								<Text style={styles.titleMessage}>
									{username}
								</Text>
							</View>
						</View>
						<View style={styles.imageflexSection}>
							<Image
								style={styles.gemsLogo}
								source={schoolLogo}
							></Image>
						</View>
					</View>
				</Card>
				<Text style={{ textAlign: "center", paddingVertical: 20 }}>
					{new Date().toDateString()}
				</Text>
				<TouchableOpacity
					style={styles.card}
					onPress={() => navigation.navigate("News")}
				>
					<ImageBackground source={newsBg} style={styles.cardBgImage}>
						<View>
							<Text style={styles.titlelabel}>News</Text>
						</View>
						<View>
							<Text style={styles.caption}>Go to News</Text>
						</View>
					</ImageBackground>
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.card}
					onPress={() => navigation.navigate("Calendar")}
				>
					<ImageBackground
						source={eventsBg}
						style={styles.cardBgImage}
					>
						<View>
							<Text style={styles.titlelabel}>Events</Text>
						</View>
						<View>
							<Text style={styles.caption}>Go to Calendar</Text>
						</View>
					</ImageBackground>
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.card}
					onPress={() => navigation.navigate("Grades")}
				>
					<ImageBackground source={gpaBg} style={styles.cardBgImage}>
						<View>
							<Text style={styles.titlelabel}>GPA</Text>
						</View>
						<View>
							<Text style={styles.caption}>
								Go to Grades & {"\n"} Transcripts
							</Text>
						</View>
					</ImageBackground>
				</TouchableOpacity>
			</ScrollView>
			<Modal
				isVisible={accountPickerVisible}
				onBackdropPress={() => setAccountPickerVisible(false)}
			>
				<View style={styles.modalView}>
					<Text
						style={{
							textAlign: "center",
							fontWeight: "bold",
							fontSize: 20,
							marginBottom: 10,
						}}
					>
						Select Profile
					</Text>
					<FlatList
						data={students}
						keyExtractor={(item) => item.id.toString()}
						renderItem={({ item, index }) => (
							<TouchableHighlight
								style={[
									{
										flexDirection: "row",
										padding: 5,
										alignItems: "center",
										marginBottom: 10,
										borderRadius: 10,
									},
									focusActiveProfile(item.id),
								]}
								underlayColor="#ddd"
								onPress={() => {
									setActiveProfile(students[index]);
									setAccountPickerVisible(false);
								}}
							>
								<>
									<Avatar.Image
										size={40}
										source={{ uri: item.image_original }}
									/>
									<Text style={{ marginLeft: 15 }}>
										{item.firstname} {item.lastname}
									</Text>
								</>
							</TouchableHighlight>
						)}
					/>
				</View>
			</Modal>
		</View>
	);
}
const styles = StyleSheet.create({
	container: {
		position: "relative",
	},
	titleMessage: {
		...Typography.blueTitlehead,
		lineHeight: 30,
	},
	title: {
		fontSize: 20,
		fontWeight: "bold",
	},
	caption: {
		...Typography.regularSmallText,
		color: Colors.darkBlueTheme,
		lineHeight: 21,
	},
	flexrowSection: {
		flexDirection: "row",
		alignItems: "center",
	},
	messageflexSection: {
		flex: 2,
	},
	imageflexSection: {
		flex: 1,
		justifyContent: "center",
	},
	gemsLogo: {
		width: 74,
		height: 84,
	},
	lfesLogo: {
		width: 74,
		height: 77,
	},
	titlelabel: {
		...Typography.blueTitlehead,
		lineHeight: 30,
		color: Colors.black,
	},
	card: {
		marginBottom: 30,
	},
	cardBgImage: {
		justifyContent: "center",
		paddingLeft: 20,
		height: 96,
		borderRadius: 25,
		overflow: "hidden",
	},
	modalView: {
		width: "100%",
		height: "50%",
		padding: 20,
		backgroundColor: "white",
		borderRadius: 20,
	},
});
