import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	TouchableOpacity,
	ScrollView,
	SafeAreaView,
	RefreshControl,
} from "react-native";
import { globalStyles } from "../styles/globalStyles";
import Card from "../components/shared/Cards";
import { Colors } from "../styles";
import { myAssignmentsAndResources } from "../api/DataApi";
import { AuthContext } from "../components/Context";
import LoadingIndicator from "../components/shared/LoadingIndicator";
import Retry from "../components/shared/Retry";
import Message from "../components/shared/Message";

export default function ResourcesSubjectsScreen({ navigation }) {
	const [isLoading, setIsLoading] = useState(true);
	const { server, userToken, activeProfile } = React.useContext(AuthContext);
	const [classes, setClasses] = useState([]);
	const [status, setStatus] = useState("");
	const [error, setError] = useState(false);
	const [message, setMessage] = useState("");
	const [retry, setRetry] = useState(false);

	const getAPI = async () => {
		setIsLoading(true);
		const response = await myAssignmentsAndResources(
			server,
			userToken,
			activeProfile.id
		);
		if (response.status == 200) {
			setStatus(response.status);
			if (!response.error) {
				setError(response.error);
				setClasses(response.data.subjects);
			} else {
				setError(response.error);
				setMessage("No data available");
			}
		} else if (response.status == "failed") {
			setStatus(response.status);
			setError(response.error);
			setMessage(response.message);
		}
		setIsLoading(false);
	};

	useEffect(() => {
		getAPI();
	}, [retry]);

	if (isLoading) {
		return <LoadingIndicator />;
	}
	return (
		<SafeAreaView style={globalStyles.container}>
			{status == 200 ? (
				<ScrollView
						refreshControl={
		          <RefreshControl
		            refreshing={isLoading}
		            onRefresh={getAPI}
		          />
		        }>
					{!error ? (
						<FlatList
							data={classes}
							keyExtractor={(item) => item.id.toString()}
							renderItem={({ item }) => (
								<TouchableOpacity
									onPress={() =>
										navigation.push("Resources", item)
									}
								>
									<Card>
										<Text style={styles.subject}>
											{item.name}
										</Text>
										<Text style={styles.caption}>
											{item.subject_code}
										</Text>
									</Card>
								</TouchableOpacity>
							)}
						/>
					) : (
						<Message message={message} />
					)}
				</ScrollView>
			) : (
				<Retry
					handleRetry={() => setRetry((prevState) => !prevState)}
					message={message}
				/>
			)}
		</SafeAreaView>
	);
}
const styles = StyleSheet.create({
	subject: {
		fontSize: 20,
		marginBottom: 5,
	},
	caption: {
		color: Colors.grayText,
	},
});
